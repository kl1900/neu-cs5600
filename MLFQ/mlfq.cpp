#include <iostream>
#include <list>
#include <iterator>
#include <map>

// define job class
class Job
{
public:
    int id;
    int currPri;
    int StartTime;
    int RunTime;
    int IOFreq;
    int TicksLeft;
    int AllotmentLeft;
    int TimeLeft;
    bool DoingIO;
    int FirstRun;
    int IOTime;
    int EndTime;

    Job(int startTime, int runTime, int ioFreq) {
        StartTime = startTime;
        RunTime = runTime;
        TimeLeft = runTime;
        IOFreq = ioFreq;
        // set a default IO time for now
        IOTime = 3;
    }
};

class Mlfq
{
public:
    int Allotment;
    int Quantum;
    int Boost;
    int HiQueue;
    std::map<int, std::list <Job>> queue;
    std::map<int, int> IODone;
    
    Mlfq(int allotment, int quantum, int boost, int numQueues)
    {
        Allotment = allotment;
        Quantum = quantum;
        Boost = boost;
        HiQueue = numQueues - 1;
        // reserve Highest queue for the incoming job list
        for (int i =0; i < HiQueue; i++) {
            std::list <Job> l;
            queue[i] = l;
        }
    }

    void run(std::list <Job> *jobs) {
        // put jobs in the top queue
        queue[HiQueue] = *jobs;

        // add other info to each job
        for (std::list<Job>::iterator it = queue[HiQueue].begin(); it != queue[HiQueue].end(); ++it){
            it -> AllotmentLeft = Allotment;
            it -> TicksLeft = Quantum;
            it -> currPri = HiQueue;
        } 

        // set up some needed values
        int finishedJobs = 0;
        int totalJobs = (*jobs).size();
        int currTime = 0;

        // print initial stats
        std::cout << "Before starting =====" << std::endl;
        for (int q = HiQueue; q >=0; q--) {
            std::cout << "Queue " << q << " contains "
            << queue[q].size() << " jobs" << std::endl;
        }
        
        std::cout << "Start running ======" << std::endl;
        // start running
        while (finishedJobs < totalJobs) {
            // finishedJobs ++;

            // check boost
            if (currTime % Boost == 0 and currTime != 0) {
                std::cout << "[ time " << currTime << " ] BOOST" <<  std::endl;
                boost(jobs);
            }

            int currQueue = FindQueue();
            std::cout << "Current queue " << currQueue << std::endl;
            if (currQueue == -1) {
                std::cout << "[ time " << currTime <<" ] IDLE" << std::endl;
                currTime++;
                return;
            }

            // there was at least one runnable job, and hence ...
            Job &currJob = *queue[currQueue].begin();
            // currJob
            currJob.TimeLeft = currJob.TimeLeft - 1;
            currJob.TicksLeft--;
            // currJob.AllotmentLeft--;

            if (currJob.FirstRun == -1) currJob.FirstRun = currTime;
            std::cout << "[ time " << currTime << " ] Run Job " << currJob.id << " at PRIORITY " << currQueue 
            << " [ TICKS " << currJob.TicksLeft << " ALLOT " << currJob.AllotmentLeft << " TIME "
            << currJob.TimeLeft << " (of " << currJob.RunTime << ") ]" << std::endl;


            // UPDATE TIME
            currTime ++;

            // CHECK FOR JOB ENDING
            if (currJob.TimeLeft == 0) {
                std::cout << "[ time " << currTime << " ] FINISHED JOB " << currJob.id << std::endl;
                finishedJobs ++;
                currJob.EndTime = currTime;
                queue[currQueue].pop_front();
            }
            
            // // TODO: CHECK FOR IO

            // CHECK FOR QUANTUM ENDING AT THIS LEVEL (BUT REMEMBER, THERE STILL MAY BE ALLOTMENT LEFT)
            if (currJob.TicksLeft == 0) {
                currJob.AllotmentLeft --;
                if (currJob.AllotmentLeft == 0) {
                    if (currQueue > 0) {
                        currJob.currPri --;
                        currJob.TicksLeft = Quantum;
                        currJob.AllotmentLeft = Allotment;
                        queue[currQueue - 1].push_front(currJob);
                        queue[currQueue].pop_front();
                    } else {
                        currJob.TicksLeft = Quantum;
                        currJob.AllotmentLeft = Allotment;
                    }
                } else {
                    currJob.TicksLeft = Quantum;
                }
            }
        }
    }


    void boost(std::list <Job> *jobs) {
        // remove all jobs from queues (except high queue) and put them in high queue
        for (int q = 0; q < HiQueue; q++){
            for (std::list<Job>::iterator it = queue[q].begin(); it != queue[q].end(); ++it){
                if (it -> DoingIO == false) queue[HiQueue].push_front(*it);
            }      
        }
        //  change priority to high priority
        //  reset number of ticks left for all jobs (just for lower jobs?)
        //  add to highest run queue (if not doing I/O)
        for (std::list<Job>::iterator it = queue[HiQueue].begin(); it != queue[HiQueue].end(); ++it){
            if (it -> TimeLeft > 0) {
                it -> currPri = HiQueue;
                it -> TicksLeft = Allotment;
            }
        }
    }

    int FindQueue() {
        int q = HiQueue;
        while (q > 0) {
            if (queue[q].size() > 0) return q;
            q--;
        }
        if (queue[0].size() == 0) return 0;
        return -1;
    }

};


int main() {

    // initialize and configure a scheduler
    Mlfq scheduler(5,3,30,3);

    // initialize a few jobs
    Job j1(0,19,0);
    Job j2(0,25,0);

    // create job array
    int jobCnt = 0;
    std::list <Job> jobs;
    j1.id = 0;
    jobs.push_front(j1);
    j2.id = 1;
    jobs.push_front(j2);
    

    // run the jobs in the scheduler
    scheduler.run(&jobs);

    return 0;
}
