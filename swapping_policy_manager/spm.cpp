#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

using namespace std;  

string hfunc(int index) {
    if (index == -1) return "MISS";
    else return "HIT";
}

string vfunc(int victim){
    if (victim == -1) return "-";
    else return to_string(victim);
}

int find_index(vector<int> memory, int item) {
    int i = 0;
    int n = memory.size();

    while (i < n)
    {
        if (memory.at(i) == item) {
            break;
        }
        i++;
    }

    if (i < n) return i;
    else return -1;
}

int main(int argc, char *argv[]) {
    int *addrListTemp;
    int s = argc - 1;
    if (argc > 1) {
        addrListTemp = (int *) malloc((argc - 1) * sizeof(int));
    } else {
        printf("needs addresses to process\n");
        exit(1);
    }

    for (int i = 0; i < s; i++) {
        addrListTemp[i] = atoi(argv[i+1]);
    }

    int cachesize = 3;
    int count = 0;
    int hits = 0;
    int miss = 0;
    string policy = "LRU";

    // accessing each address
    vector<int> memory;
    for (int i = 0; i < s; i++) {
        int n = addrListTemp[i];
        int idx = find_index(memory, n);
        if (idx > -1) {
            hits ++;
            if (policy == "LRU") {
                memory.erase(memory.begin() + idx);
                memory.push_back(n);
            }
        }
        // generate a miss
        else {
            miss ++;
        }


        int victim = -1;
        if (idx == -1) {
            if (count == cachesize) {
                if (policy == "LRU" or policy == "FIFO") {
                    victim = memory.at(0);
                    memory.erase(memory.begin());
                }
                
            } else {
                count ++;
            }
            memory.push_back(n);
        }
        cout << "Access: " << n << " " << hfunc(idx) << " Memory:";
        for (int i = 0; i < memory.size(); i++) {
            cout << " " << memory.at(i);
        }
        cout << endl;
    }


    cout << "FINALSTATS hits " << hits << " misses "<< miss << " hit rate " << (double) hits/(hits + miss) << endl;
    free(addrListTemp);
    return 0;
}
