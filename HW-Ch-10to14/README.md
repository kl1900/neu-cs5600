## CH 10 Questions 
#### 1. To start things off, let’s learn how to use the simulator to study how to build an effective multi-processor scheduler. The first simulation will run just one job, which has a run-time of 30, and a working-set size of 200. Run this job (called job ’a’ here) on one simulated CPU as follows: ./multi.py -n 1 -L a:30:200. How long will it take to complete? Turn on the -c flag to see a final answer, and the -t flag to see a tick-by-tick trace of the job and how it is scheduled. 

30 ticks. We have only 1 cpu, and 1 job that takes 30 ticks

#### 2. Now increase the cache size so as to make the job’s working set (size=200) fit into the cache (which, by default, is size=100); for example, run ./multi.py -n 1 -L a:30:200 -M 300. Can you predict how fast the job will run once it fits in cache? (hint: remember the key parameter of the warm rate, which is set by the -r flag) Check your answer by running with the solve flag (-c) enabled. 

20 ticks. first 10 ticks to warm up the cache, then with 2x speed taking 10 ticks to finish the 20-tick remaining job.


#### 3. One cool thing about multi.py is that you can see more detail about what is going on with different tracing flags. Run the same simulation as above, but this time with time left tracing enabled (-T). This flag shows both the job that was scheduled on a CPU at each time step, as well as how much run-time that job has left after each tick has run. What do you notice about how that second column decreases? 

The second column decreases faster after 10 ticks, because the cpu is warmed up.

#### 4. Now add one more bit of tracing, to show the status of each CPU cache for each job, with the -C flag. For each job, each cache will either show a blank space (if the cache is cold for that job) or a ’w’ (if the cache is warm for that job). At what point does the cache become warm for job ’a’ in this simple example? What happens as you change the warmup time parameter (-w) to lower or higher values than the default? 

At tick 9 the cpu becomes warm to job a.

`-w` flag controls how many ticks it takes for the cpu to warm up to a job. Smaller `-w` value means the cpu warms up quicker, and vise versa


#### 5. At this point, you should have a good idea of how the simulator works for a single job running on a single CPU. But hey, isn’t this a multi-processor CPU scheduling chapter? Oh yeah! So let’s start working with multiple jobs. Specifically, let’s run the following three jobs on a two-CPU system (i.e., type ./multi.py -n 2 -L a:100:100,b:100:50,c:100:50) Can you predict how long this will take, given a round-robin centralized scheduler? Use -c to see if you were right, and then dive down into details with -t to see a step-by-step and then -C to see whether caches got warmed effectively for these jobs. What do you notice? 

I guess it takes 150 ticks to finish, the total job length is 300, divided by 2 cpus, becomes 150. 

Using `-C` `-T` and `-t` to trace, it's shown that no cpu is warmed up to any job because the quantum time is set to be the same as warm up time, and therefore the job is switched out before the speed up takes place.


#### 6. Now we’ll apply some explicit controls to study cache affinity, as described in the chapter. To do this, you’ll need the -A flag. This flag can be used to limit which CPUs the scheduler can place a particular job upon. In this case, let’s use it to place jobs ’b’ and ’c’ on CPU 1, while restricting ’a’ to CPU 0. This magic is accomplished by typing this ./multi.py -n 2 -L a:100:100,b:100:50, c:100:50 -A a:0,b:1,c:1 ; don’t forget to turn on various tracing options to see what is really happening! Can you predict how fast this version will run? Why does it do better? Will other combinations of ’a’, ’b’, and ’c’ onto the two processors run faster or slower? 

It takes 110 ticks. Job b and c both takes 10 ticks to warms up and 45 ticks to finish, so each takes 55 ticks. Two makes 110 ticcks.

Other combos will be slower. Job b and c can share a cpu and achieve consistent warmed up cpu is because the sum of their cache requirement is <= the cache of the cpu. And therefore the cpu can keep both of the jobs cached without cooling down when they do round-robin.


#### 7. One interesting aspect of caching multiprocessors is the opportunity for better-than-expected speed up of jobs when using multiple CPUs (and their caches) as compared to running jobs on a single processor. Specifically, when you run on N CPUs, sometimes you can speed up by more than a factor of N, a situation entitled super-linear speedup. To experiment with this, use the job description here (-L a:100:100,b:100:100,c:100:100) with a small cache (-M 50) to create three jobs. Run this on systems with 1, 2, and 3 CPUs (-n 1, -n 2, -n 3). Now, do the same, but with a larger per-CPU cache of size 100. What do you notice about performance as the number of CPUs scales? Use -c to confirm your guesses, and other tracing flags to dive even deeper. 
```
./multi.py -L a:100:100,b:100:100,c:100:100 -M 50 -n 1 -c
Finished time 300

./multi.py -L a:100:100,b:100:100,c:100:100 -M 50 -n 2 -c
Finished time 150

./multi.py -L a:100:100,b:100:100,c:100:100 -M 50 -n 3 -c
Finished time 100

./multi.py -L a:100:100,b:100:100,c:100:100 -c -M 100 -n 1
Finished time 300

./multi.py -L a:100:100,b:100:100,c:100:100 -c -M 100 -n 2
Finished time 150

./multi.py -L a:100:100,b:100:100,c:100:100 -c -M 100 -n 3
Finished time 55
```
The super-leaner speed up is achieved, only when the cache size is enough for each jobs to achieve warm up, and the fact that there are enough cpus for each job, so no round-robin is needed.



#### 8. One other aspect of the simulator worth studying is the per-CPU scheduling option, the -p flag. Run with two CPUs again, and this three job configuration (-L a:100:100,b:100:50,c:100:50). How does this option do, as opposed to the hand-controlled affinity limits you put in place above? How does performance change as you alter the ’peek interval’ (-P) to lower or higher values? How does this per-CPU approach work as the number of CPUs scales? 
 the `-p` flag creates multiple queues for each cpu, and tries to distribute number of jobs evenly to each cpu.

 smaller `-P` value occassionally increase the effeciency, because it means the idle cpu might be able to steal other cpu's jobs quicker.




#### 9. Finally, feel free to just generate random workloads and see if you can predict their performance on different numbers of processors, cache sizes, and scheduling options. If you do this, you’ll soon be a multi-processor scheduling master, which is a pretty awesome thing to be. Good luck!
```
./multi.py -L a:100:100,b:100:50,c:100:50 -p -c -P 5
...
Scheduler CPU 0 queue: ['a', 'c']
Scheduler CPU 1 queue: ['b']


Finished time 90
```


## CH 13 Questions
#### 1. The first Linux tool you should check out is the very simple tool free. First, type man free and read its entire manual page; it’s short, don’t worry!


#### 2. Now, run free, perhaps using some of the arguments that might be useful (e.g., -m, to display memory totals in megabytes). How much memory is in your system? How much is free? Do these numbers match your intuition?
```
free -m
              total        used        free      shared  buff/cache   available
Mem:          23916        3880        4797         701       15239       18944
Swap:          4095           0        4095
```
Yes, I have excessive amount of memory installed.

#### 3. Next, create a little program that uses a certain amount of memory, called memory-user.c. This program should take one commandline argument: the number of megabytes of memory it will use. When run, it should allocate an array, and constantly stream through the array, touching each entry. The program should do this indefinitely, or, perhaps, for a certain amount of time also specified at the command line.
see [memory-user.c](memory-user.c)


#### 4. Now, while running your memory-user program, also (in a different terminal window, but on the same machine) run the free tool. How do the memory usage totals change when your program is running? How about when you kill the memory-user program? Do the numbers match your expectations? Try this for different amounts of memory usage. What happens when you use really large amounts of memory?
yes it changes, and it matches my expectation.
when using large amounts of memory, the memory usage caps at around ~800 MB


#### 5. Let’s try one more tool, known as pmap. Spend some time, and read the pmap manual page in detail. 

#### 6. To use pmap, you have to know the process ID of the process you’re interested in. Thus, first run ps auxw to see a list of all processes; then, pick an interesting one, such as a browser. You can also use your memory-user program in this case (indeed, you can even have that program call getpid() and print out its PID for your convenience).
```
pmap 131135
131135:   ./memory-user 10
000055ba85b39000      4K r---- memory-user
000055ba85b3a000      4K r-x-- memory-user
000055ba85b3b000      4K r---- memory-user
000055ba85b3c000      4K r---- memory-user
000055ba85b3d000      4K rw--- memory-user
000055ba85c7b000    132K rw---   [ anon ]
00007f0c55d62000  10252K rw---   [ anon ]
00007f0c56765000    152K r---- libc-2.33.so
00007f0c5678b000   1452K r-x-- libc-2.33.so
00007f0c568f6000    304K r---- libc-2.33.so
00007f0c56942000     12K r---- libc-2.33.so
00007f0c56945000     12K rw--- libc-2.33.so
00007f0c56948000     44K rw---   [ anon ]
00007f0c56966000      4K r---- ld-2.33.so
00007f0c56967000    156K r-x-- ld-2.33.so
00007f0c5698e000     40K r---- ld-2.33.so
00007f0c56998000      8K r---- ld-2.33.so
00007f0c5699a000      8K rw--- ld-2.33.so
00007ffdd750f000    136K rw---   [ stack ]
00007ffdd758c000     16K r----   [ anon ]
00007ffdd7590000      8K r-x--   [ anon ]
ffffffffff600000      4K --x--   [ anon ]
 total            12760K

```


#### 7. Now run pmap on some of these processes, using various flags (like -X) to reveal many details about the process. What do you see? How many different entities make up a modern address space, as opposed to our simple conception of code/stack/heap?
```
pmap 131135 -X
131135:   ./memory-user 10
         Address Perm   Offset Device    Inode  Size   Rss   Pss Referenced Anonymous LazyFree ShmemPmdMapped FilePmdMapped Shared_Hugetlb Private_Hugetlb Swap SwapPss Locked THPeligible Mapping
    55ba85b39000 r--p 00000000  fd:01 14559443     4     4     4          4         0        0              0             0              0               0    0       0      0           0 memory-user
    55ba85b3a000 r-xp 00001000  fd:01 14559443     4     4     4          4         0        0              0             0              0               0    0       0      0           0 memory-user
    55ba85b3b000 r--p 00002000  fd:01 14559443     4     4     4          4         0        0              0             0              0               0    0       0      0           0 memory-user
    55ba85b3c000 r--p 00002000  fd:01 14559443     4     4     4          4         4        0              0             0              0               0    0       0      0           0 memory-user
    55ba85b3d000 rw-p 00003000  fd:01 14559443     4     4     4          4         4        0              0             0              0               0    0       0      0           0 memory-user
    55ba85c7b000 rw-p 00000000  00:00        0   132     4     4          4         4        0              0             0              0               0    0       0      0           0 [heap]
    7f0c55d62000 rw-p 00000000  00:00        0 10252 10248 10248      10248     10248        0              0             0              0               0    0       0      0           0 
    7f0c56765000 r--p 00000000  fd:01 22545369   152   152     1        152         0        0              0             0              0               0    0       0      0           0 libc-2.33.so
    7f0c5678b000 r-xp 00026000  fd:01 22545369  1452   980     7        980         0        0              0             0              0               0    0       0      0           0 libc-2.33.so
    7f0c568f6000 r--p 00191000  fd:01 22545369   304   168     1        168         0        0              0             0              0               0    0       0      0           0 libc-2.33.so
    7f0c56942000 r--p 001dc000  fd:01 22545369    12    12    12         12        12        0              0             0              0               0    0       0      0           0 libc-2.33.so
    7f0c56945000 rw-p 001df000  fd:01 22545369    12    12    12         12        12        0              0             0              0               0    0       0      0           0 libc-2.33.so
    7f0c56948000 rw-p 00000000  00:00        0    44    20    20         20        20        0              0             0              0               0    0       0      0           0 
    7f0c56966000 r--p 00000000  fd:01 22545179     4     4     0          4         0        0              0             0              0               0    0       0      0           0 ld-2.33.so
    7f0c56967000 r-xp 00001000  fd:01 22545179   156   156     1        156         0        0              0             0              0               0    0       0      0           0 ld-2.33.so
    7f0c5698e000 r--p 00028000  fd:01 22545179    40    40     0         40         0        0              0             0              0               0    0       0      0           0 ld-2.33.so
    7f0c56998000 r--p 00031000  fd:01 22545179     8     8     8          8         8        0              0             0              0               0    0       0      0           0 ld-2.33.so
    7f0c5699a000 rw-p 00033000  fd:01 22545179     8     8     8          8         8        0              0             0              0               0    0       0      0           0 ld-2.33.so
    7ffdd750f000 rw-p 00000000  00:00        0   136    16    16         16        16        0              0             0              0               0    0       0      0           0 [stack]
    7ffdd758c000 r--p 00000000  00:00        0    16     0     0          0         0        0              0             0              0               0    0       0      0           0 [vvar]
    7ffdd7590000 r-xp 00000000  00:00        0     8     4     0          4         0        0              0             0              0               0    0       0      0           0 [vdso]
ffffffffff600000 --xp 00000000  00:00        0     4     0     0          0         0        0              0             0              0               0    0       0      0           0 [vsyscall]
                                               ===== ===== ===== ========== ========= ======== ============== ============= ============== =============== ==== ======= ====== =========== 
                                               12760 11852 10358      11852     10336        0              0             0              0               0    0       0      0           0 KB 

```

#### 8. Finally, let’s run pmap on your memory-user program, with different amounts of used memory. What do you see here? Does theoutput from pmap match your expectations?
Yes it matches :)

## CH 14 Question

#### 1. First, write a simple program called null.c that creates a pointer to an integer, sets it to NULL, and then tries to dereference it. Compile this into an executable called null. What happens when you run this program?
after compile [null.c](null.c) it has a this crash
```
./null
Segmentation fault (core dumped)
```

#### 2. Next, compile this program with symbol information included (with the -g flag). Doing so let’s put more information into the executable, enabling the debugger to access more useful information about variable names and the like. Run the program under the de bugger by typing gdb null and then, once gdb is running, typing run. What does gdb show you?

```sh
gdb null
(gdb) run
Program received signal SIGSEGV, Segmentation fault.
0x00005555555551b4 in main () at null.c:7
7           printf("%d\n", *x);
```

it shows which line causes the SIGSEGV.



#### 3. Finally, use the valgrind tool on this program. We’ll use the memcheck tool that is a part of valgrind to analyze what happens. Run this by typing in the following: `valgrind --leak-check=yes ./null`. What happens when you run this? Can you interpret the output from the tool?

```
valgrind --leak-check=yes ./null
==117657== Memcheck, a memory error detector
==117657== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==117657== Using Valgrind-3.17.0 and LibVEX; rerun with -h for copyright info
==117657== Command: ./null
==117657== 
==117657== Invalid read of size 4
==117657==    at 0x1091AF: main (null.c:7)
==117657==  Address 0x0 is not stack'd, malloc'd or (recently) free'd
==117657== 
==117657== 
==117657== Process terminating with default action of signal 11 (SIGSEGV)
==117657==  Access not within mapped region at address 0x0
==117657==    at 0x1091AF: main (null.c:7)
==117657==  If you believe this happened as a result of a stack
==117657==  overflow in your program's main thread (unlikely but
==117657==  possible), you can try to increase the size of the
==117657==  main thread stack using the --main-stacksize= flag.
==117657==  The main thread stack size used in this run was 8388608.
==117657== 
==117657== HEAP SUMMARY:
==117657==     in use at exit: 4 bytes in 1 blocks
==117657==   total heap usage: 1 allocs, 0 frees, 4 bytes allocated
==117657== 
==117657== 4 bytes in 1 blocks are definitely lost in loss record 1 of 1
==117657==    at 0x4842839: malloc (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
==117657==    by 0x10919E: main (null.c:5)
==117657== 
==117657== LEAK SUMMARY:
==117657==    definitely lost: 4 bytes in 1 blocks
==117657==    indirectly lost: 0 bytes in 0 blocks
==117657==      possibly lost: 0 bytes in 0 blocks
==117657==    still reachable: 0 bytes in 0 blocks
==117657==         suppressed: 0 bytes in 0 blocks
==117657== 
==117657== For lists of detected and suppressed errors, rerun with: -s
==117657== ERROR SUMMARY: 2 errors from 2 contexts (suppressed: 0 from 0)
Segmentation fault (core dumped)
```
it says which line contains variable that is not stack'd, malloc'd or free'd
```
==117657== Invalid read of size 4
==117657==    at 0x1091AF: main (null.c:7)
==117657==  Address 0x0 is not stack'd, malloc'd or (recently) free'd
```


#### 4. Write a simple program that allocates memory using malloc() but forgets to free it before exiting. What happens when this program runs? Can you use gdb to find any problems with it? How about valgrind (again with the --leak-check=yes flag)? 
program is [forget_to_free.c](forget_to_free.c)

gdb did not find any problems with it
```
Reading symbols from forget_to_free...
(gdb) run
Starting program: forget_to_free 
int is 1
[Inferior 1 (process 118439) exited normally]
```

valgrind find the problem
```
==118566== HEAP SUMMARY:
==118566==     in use at exit: 4 bytes in 1 blocks
==118566==   total heap usage: 2 allocs, 1 frees, 1,028 bytes allocated
==118566== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

#### 5. Write a program that creates an array of integers called data of size 100 using malloc; then, set data[100] to zero. What happens when you run this program? What happens when you run this program using valgrind? Is the program correct?
Program is [array_of_int.c](array_of_int.c)

when run, nothing happens. But valgrind detects the error:
```
valgrind --leak-check=yes ./array_of_int 
==119679== Invalid write of size 4
==119679==    at 0x10918D: main (array_of_int.c:6)
==119679==  Address 0x4a571d0 is 0 bytes after a block of size 400 alloc'd
```


#### 6. Create a program that allocates an array of integers (as above), frees them, and then tries to print the value of one of the elements of the array. Does the program run? What happens when you use valgrind on it?
Program is [print_int.c](print_int.c)
```sh
./"print_int" 
first element is 1680283648
```
valgrind shows invalid size error:
```
==120193== Invalid read of size 4
==120193==    at 0x1091B3: main (print_int.c:7)
==120193==  Address 0x4a57040 is 0 bytes inside a block of size 400 free'd
```


#### 7. Now pass a funny value to free (e.g., a pointer in the middle of the array you allocated above). What happens? Do you need tools to find this type of problem? 
Program is [funny_value.c](funny_value.c)
No tool is needed, error happens at run time:
```
./"funny_value" 
free(): invalid pointer
Aborted (core dumped)
```

#### 8. Try out some of the other interfaces to memory allocation. For example, create a simple vector-like data structure and related routines that use realloc() to manage the vector. Use an array to store the vectors elements; when a user adds an entry to the vec tor, use realloc() to allocate more space for it. How well does such a vector perform? How does it compare to a linked list? Use valgrind to help you find bugs.

see [vector.c](vector.c)


#### 9. Spend more time and read about using gdb and valgrind. Knowing your tools is critical; spend the time and learn how to become an expert debugger in the UNIX and C environment.
