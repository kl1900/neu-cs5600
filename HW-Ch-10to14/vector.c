// largely based on this tutorial https://aticleworld.com/implement-vector-in-c/,
// but fixed some bugs
#include <stdio.h>
#include <stdlib.h>


#define VECTOR_INIT_CAPACITY 1
#define UNDEFINE -1
#define SUCCESS 0

typedef struct VectorList
{
    void **items;
    int total;
    int capacity;
} VectorList;

//structure contain the function pointer
typedef struct Vector vector;

struct Vector
{
    VectorList vectorList;
    //function pointers
    int (*fVectorTotal)(vector *);
    int (*fVectorResize)(vector *, int);
    int (*fVectorAdd)(vector *, void *);
    int (*fVectorSet)(vector *, int, void *);
    void *(*fVectorGet)(vector *, int);
    int (*fVectorDelete)(vector *, int);
    int (*fVectorFree)(vector *);
};



int vectorResize(vector *v, int capacity) {
    int  status = UNDEFINE;
    if(v)
    {
        void **items = (void**) realloc(v->vectorList.items, sizeof(void *) * capacity);
        if (items)
        {
            v->vectorList.items = items;
            v->vectorList.capacity = capacity;
            status = SUCCESS;
        }
    }
    return status;
}

int vectorTotal(vector* v) {
    int count = UNDEFINE;
    if (v) {
        count = v ->vectorList.total;
    }
    return count;
}

int vectorAdd(vector *v, void *item) {
    int status = UNDEFINE;
    if (v) {
        // check if the capacity is full
        if (v -> vectorList.total == v -> vectorList.capacity) {
            // first resize the vector
            status = vectorResize(v, v->vectorList.capacity + 1);
            if (status == SUCCESS) {
                v->vectorList.items[v->vectorList.total++] = item;
            }
        }
        else {
            v->vectorList.items[v->vectorList.total] = item;
            v->vectorList.total++;
            status = SUCCESS;
        }
    }
    return status;
}

int vectorSet(vector *v, int index,void *item) {
    int status = UNDEFINE;
    if (v) {
        if ((index >= 0) && (index < v->vectorList.total)) {
            v->vectorList.items[index] = item;
            status = SUCCESS;
        }
    }
    return status;
}

// get the address of item with given index
void *vectorGet(vector *v,int index) {
    void *data = NULL;
    if (v) {
        if ((index < v->vectorList.total) && (index >=0)) {
            data = v->vectorList.items[index];
        }
    }
    return data;
}

int vectorDelete(vector *v, int index)
{
    int  status = UNDEFINE;
    int i = 0;
    if(v)
    {
        if ((index < 0) || (index >= v->vectorList.total))
            return status;
        v->vectorList.items[index] = NULL;
        for (i = index; (i < v->vectorList.total - 1); ++i)
        {
            v->vectorList.items[i] = v->vectorList.items[i + 1];
            v->vectorList.items[i + 1] = NULL;
        }
        v->vectorList.total--;
        if ((v->vectorList.total > 0) && ((v->vectorList.total) == (v->vectorList.capacity / 4)))
        {
            vectorResize(v, v->vectorList.capacity / 2);
        }
        status = SUCCESS;
    }
    return status;
}

int vectorFree(vector *v)
{
    int  status = UNDEFINE;
    if(v)
    {
        free(v->vectorList.items);
        v->vectorList.items = NULL;
        status = SUCCESS;
    }
    return status;
}

// create vector
void vec_constructor(vector *v) {
    //function pointers
    v->fVectorTotal = vectorTotal;
    v->fVectorResize = vectorResize;
    v->fVectorAdd = vectorAdd;
    v->fVectorSet = vectorSet;
    v->fVectorGet = vectorGet;
    v->fVectorFree = vectorFree;
    v->fVectorDelete = vectorDelete;

    //initialize the capacity and allocate the memory
    v->vectorList.capacity = VECTOR_INIT_CAPACITY;
    v->vectorList.total = 0;
    v->vectorList.items = (void**) malloc(sizeof(void *) * v->vectorList.capacity);
}

int main() {
    struct Vector v;
    vec_constructor(&v);
    char *hello = "hello world\n";
    char *hello2 = "hello world 2\n";
    v.fVectorAdd(&v, hello);
    v.fVectorAdd(&v, hello2);
    v.fVectorResize(&v,10);
    printf("%s",v.fVectorGet(&v, 1));
    v.fVectorDelete(&v,1);
    v.fVectorFree(&v);
    return 0;
}

