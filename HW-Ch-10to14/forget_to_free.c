#include <stdio.h>
#include <stdlib.h>

int main() {
    int *x = (int *) malloc(sizeof(int));
    *x = 1;
    printf("int is %d\n", *x);
    return 0;
}