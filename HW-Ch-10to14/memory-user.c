#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <unistd.h>

double count_time(clock_t start) {
    return ((double)(clock() - start)) / CLOCKS_PER_SEC;
}

int main(int argc, char **argv)
{
    if (argc > 3 || argc == 1)
    {
        printf("Usage: <number of megabytes> (optional)<run time in seconds>\n");
        exit(EXIT_FAILURE);
    }
    int num_of_bytes = atoi(argv[1]) * 1024 * 1024;
    int total_time;
    if (argc == 3) {
        total_time = atoi(argv[2]);
    } else {
        total_time = INT_MAX;
    }
    
    int *x = malloc(num_of_bytes);
    int length = (int)(num_of_bytes / sizeof(int));
    clock_t start = clock();
    double run_time = count_time(start);
    int i = 0;
    printf("pid: %d\n", getpid());
    while (run_time < total_time)
    {
        run_time = count_time(start);
        if (i == length) i = 0;
        x[i] += 0;
        i ++;
    }

    free(x);
    return 0;
}
