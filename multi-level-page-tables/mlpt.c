// This is a simulation of a 2-level page table implementation 
// A real virtual address has 32 bits: 10 bits for first table, 
// 10 bits for second, then 12 bits for off-set
//
// For simplification, this program assumes 12-bit virtual addresses:
// the first table, second table, and off-set each has 4 bits

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// last 4 bits are the off set
#define PAGE_OFFSET 0x00f
// first 4 bits is the for 1st page table
#define FIRST_TABLE_MASK 0xf00
// second 4 bits are for the 2nd page table
#define SECOND_TABLE_MASK 0x0f0
// some helpful functions
#define GET_OFFSET(number) (number & PAGE_OFFSET)
#define GET_FIRST_TABLE(number) ((number & FIRST_TABLE_MASK) >> 8)
#define GET_SECOND_TABLE(number) ((number & SECOND_TABLE_MASK) >> 4)
#define CHECK_SIZE(number) ((0xfff >= number) && (number >= 0x0ff))


int main() {
    int first_table[3], second_table[3];

    // set up tables
    first_table[0] = 0;
    first_table[1] = 1;
    first_table[2] = 2;

    second_table[0] = 0x1c;
    second_table[1] = 0x1d;
    second_table[2] = 0x1e;

    // set up a sample virtual address for demo
    unsigned int v_address;
    v_address = 0x12a;

    if(!CHECK_SIZE(v_address)) {
        printf("ERROR: virtual address %d is not legal size\n", v_address);
        exit(EXIT_FAILURE);
    }

    int first_table_address = GET_FIRST_TABLE(v_address);
    int second_table_address = GET_SECOND_TABLE(v_address);
    int off_set = GET_OFFSET(v_address);
    printf("virtual address is 0x%01x\n",v_address);
    printf("first table bit is 0x%01x\n",first_table_address);
    printf("second table bit is 0x%01x\n",second_table_address);
    printf("off set bit is 0x%01x\n",off_set);

    printf("=======================\n");

    // say 
    // first table 0,1,2
    // second table 100,200,300
    // second table address,2

    int first_address = first_table[first_table_address];
    int page_address = (second_table[first_address]) << 4;
    int physical_address = page_address + off_set;
    printf("first bit is 0x%01x\n", first_address);
    printf("second table bit is 0x%01x\n", page_address);
    printf("off set bit is 0x%01x\n", off_set);
    printf("physical address is 0x%01x\n", physical_address);
}

