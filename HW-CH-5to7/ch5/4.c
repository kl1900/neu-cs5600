#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    if (fork() == 0) {
        // child (new process)
        printf("hello, I am child (pid:%d)\n", (int) getpid());
        char *myargs[3];
        myargs[0] = strdup("ls"); // program: "ls"
        myargs[1] = strdup("./"); // argument: current directory
        myargs[2] = NULL; // marks end of array
        execvp(myargs[0], myargs); // runs command
    } 

    else {
        printf("hello, I am parent");
    }
    return 0;
}
