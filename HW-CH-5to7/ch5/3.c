#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<signal.h>


void sig_handler(int sig)
{
    printf("goodbye\n");
}
  
int main(int argc, char *argv[])
{
    int rc = fork();
    int parent = getpid();
    if (rc < 0) {
        // fork failed; exit
        fprintf(stderr, "fork failed\n");
        exit(1);
    } else if (rc == 0) {
        // child (new process)
        printf("hello\n");
    } else {
        // parent goes down this path (original process)
        sleep(1);
        printf("goodbye\n");
    }
    return 0;
}
