#include <stdio.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <assert.h>

// inspired by https://www.geeksforgeeks.org/input-output-system-calls-c-create-open-close-read-write/
int main()
{
    int fd = open("./foo.txt", O_WRONLY);
    assert(fd >= 0);
    int rc = fork();
    if (rc < 0) {
        fprintf(stderr, "fork failed\n");
        exit(1);
    } else if (rc ==0) {
        // child process
        printf("child (pid:%d) writing to file descriptor: %d\n", (int) getpid(), fd);
        int sz = write(fd, "hello from child\n", strlen("hello from child\n"));
    } else {
        printf("parent (pid:%d) writing to file descriptor: %d\n", (int) getpid(), fd);
        int sz = write(fd, "hello from parent\n", strlen("hello from parent\n"));
    }

    close(fd);
    return 0;
}
