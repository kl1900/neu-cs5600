#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    int x = 100;
    int rc = fork();
    if (rc < 0) {
        // fork failed; exit
        fprintf(stderr, "fork failed\n");
        exit(1);
    } else if (rc == 0) {
        // child (new process)
        x = 101;
        printf("hello, I am child (pid:%d), and x value is %d\n", (int) getpid(), x);
    } else {
        // parent goes down this path (original process)
        int wc = wait(NULL);
        x = 102;
        printf("hello, I am parent of %d (wc:%d) (pid:%d), and x value is %d\n",
	       rc, wc, (int) getpid(), x);
    }
    return 0;
}
