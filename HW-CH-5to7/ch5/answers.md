# Chapter 5  

### 1. Run ./fork.py -s 10 and see which actions are taken. Can you predict what the process tree looks like at each step? Use the -c flag to check your answers. Try some different random seeds (-s) or add more actions (-a) to get the hang of it.   
```
python fork.py -s 10

                           Process Tree:
                               a

Action: a forks b
Process Tree?
Action: a forks c
Process Tree?
Action: c EXITS
Process Tree?
Action: a forks d
Process Tree?
Action: a forks e
Process Tree?
```
I think the process tree looks like:
```
Action: a forks b
a
|-b
Action: a forks c
a
|-b
|-c
Action: c EXITS
a
|-b
Action: a forks d
a
|-b
|-d
Action: a forks e
a
|-b
|-d
|-e
```
the `-c` shows:
```
                           Process Tree:
                               a

Action: a forks b
                               a
                               └── b
Action: a forks c
                               a
                               ├── b
                               └── c
Action: c EXITS
                               a
                               └── b
Action: a forks d
                               a
                               ├── b
                               └── d
Action: a forks e
                               a
                               ├── b
                               ├── d
                               └── e
```
which agrees with me.

### 2. One control the simulator gives you is the fork percentage, controlled by the -f flag. The higher it is, the more likely the next action is a fork; the lower it is, the more likely the action is an exit. Run the simulator with a large number of actions (e.g., -a 100) and vary the fork percentage from 0.1 to 0.9. What do you think the resulting final process trees will look like as the percentage changes? Check your answer with -c. 

Once the fork percentage is below 0.5, it is more likely to EXIT than FORKING, causing even existing forks to terminate. So once `-f` is below 0.5, the tree is likely only having a root and/or a couple more forked branches remaining . And vise versa, if above 0.5, the tree has many many forked branches.

```
python fork.py -a 100 -f 0.3 -c
.....
Action: a forks Y
                               a
                               └── Y
Action: Y forks Z
                               a
                               └── Y
                                   └── Z

python fork.py -a 100 -f 0.8 -c
.....
Action: w forks ax
                               a
                               ├── A
                               │   ├── F
                               │   ├── I
                               │   │   └── an
                               │   └── am
                               ├── e
                               ├── h
                               ├── J
                               │   └── ac
                               ├── v
                               │   ├── ad
                               │   └── at
                               ├── z
                               │   └── aj
                               ├── E
                               ├── g
                               │   └── aq
                               ├── s
                               │   └── S
                               │       └── ah
                               ├── M
                               │   └── T
                               ├── t
                               │   ├── Z
                               │   │   ├── ae
                               │   │   └── av
                               │   └── ak
                               ├── n
                               ├── w
                               │   ├── ao
                               │   ├── ap
                               │   ├── au
                               │   └── ax
                               ├── y
                               ├── H
                               │   ├── al
                               │   └── aw
                               ├── B
                               ├── N
                               │   └── W
                               ├── X
                               ├── ab
                               ├── O
                               ├── L
                               ├── P
                               ├── ag
                               ├── R
                               ├── U
                               ├── as
                               └── V
```



### 3. Now, switch the output by using the -t flag (e.g., run ./fork.py -t). Given a set of process trees, can you tell which actions were taken? 

The `-t` flag sets reverse action, basically draws the tree and let the user guess what action has taken.

### 4. One interesting thing to note is what happens when a child exits; what happens to its children in the process tree? To study this, let’s create a specific example: ./fork.py -A a+b,b+c,c+d,c+e,c-. This example has process ’a’ create ’b’, which in turn creates ’c’, which then creates ’d’ and ’e’. However, then, ’c’ exits. What do you think the process tree should like after the exit? What if you use the -R flag? Learn more about what happens to orphaned processes on your own to add more context. 
I guess existing 'c' would cause 'd' and 'e' EXIT as well. And looks like I was wrong, and the branches uses root as their parent:
```
Action: c EXITS
                               a
                               ├── b
                               ├── d
                               └── e
```
Using the `-R` flag would reparent to local parent, so it would find `c`'s parent `b` as their parents.

```
Action: c EXITS
                               a
                               └── b
                                   ├── d
                                   └── e
```

### 5. One last flag to explore is the -F flag, which skips intermediate steps and only asks to fill in the final process tree. Run ./fork.py -F and see if you can write down the final tree by looking at the series of actions generated. Use different random seeds to try this a few times. 
```
python ./fork.py -F
Action: a forks b
Action: a forks c
Action: b forks d
Action: d EXITS
Action: c forks e

                        Final Process Tree?
```
the tree is
```
a
|-b
|-c
  |-e
```
### 6. Finally, use both -t and -F together. This shows the final process tree, but then asks you to fill in the actions that took place. By looking at the tree, can you determine the exact actions that took place? In which cases can you tell? In which can’t you tell? Try some different random seeds to delve into this question.
```
python ./fork.py -F -t
                           Process Tree:
                               a

Action?
Action?
Action?
Action?
Action?

                        Final Process Tree:
                               a
                               └── c
                                   ├── d
                                   └── e
```
the action is: `a+c,c+d,c+e`. The `b` branch was created but exited at some point, but cannot be determined.

