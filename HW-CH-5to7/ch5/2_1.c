#include <stdio.h>
#include <fcntl.h>
#include <assert.h>

// inspired by https://www.geeksforgeeks.org/input-output-system-calls-c-create-open-close-read-write/
int main()
{
    int fd = open("./foo.txt", O_RDONLY|O_CREAT);
    assert(fd >= 0);
    int rc = fork();
    if (rc < 0) {
        fprintf(stderr, "fork failed\n");
        exit(1);
    } else if (rc ==0) {
        // child process
        printf("child (pid:%d) access file descriptor: %d\n", (int) getpid(), fd);
    } else {
        printf("parent (pid:%d) access file descriptor: %d\n", (int) getpid(), fd);
    }

    close(fd);
    return 0;
}
