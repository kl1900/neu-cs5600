#define _GNU_SOURCE
#include <sys/time.h>
#include <fcntl.h>
#include <stdio.h>
#include <assert.h>
#include <sched.h>

int main() {
    measure_sys_call();
    measure_context_switching();
    return 0;
}

void measure_sys_call() {
    printf("Start measuring system call...\n");

    struct timeval start, finish;

    // open a file descriptor and assert it succeed
    int fd = open("foo.txt", O_RDONLY | O_CREAT);
    assert(fd >= 0);

    // perform read system call 10,000,000 times
    int num_of_calls = 10000000;

    gettimeofday(&start, NULL);
    for (int i = 0; i < num_of_calls; i = i + 1){
        read(fd,NULL,0);
    }
    gettimeofday(&finish, NULL);

    // calculate time passed by
    int sec_duration = finish.tv_sec - start.tv_sec;
    int micro_duration = finish.tv_usec - start.tv_usec;
    float average = ((float) (sec_duration * 1000000 + micro_duration)) / num_of_calls;

    printf("in average system call is %f microseconds long\n", average);

    close(fd);
}

void measure_context_switching() {
    printf("Start measuring context switching...\n");
    // set up pipes
    int fd1[2], fd2[2];
    assert(pipe(fd1) == 0);
    assert(pipe(fd2) == 0);

    struct timeval start, finish;
    int num_of_calls = 10000000;

    // set up cpu to one cpu only
    cpu_set_t set;
    CPU_ZERO(&set);
    CPU_SET(0, &set);


    switch (fork())
    {
        case -1:
            /* code */
            perror("fork");
        
        case 0:
            // child process runs on the set cpu
            if (sched_setaffinity(getpid(),sizeof(set), &set) == -1) {
                perror("child sched_setaffinity");
            }
            for (int i = 0; i < num_of_calls; i = i + 1){
                read(fd1[0], NULL, 0);
                write(fd2[1], NULL, 0);
            }
            break;
            
        default:
            // parent process runs here
            if (sched_setaffinity(getpid(),sizeof(set), &set) == -1) {
                perror("child sched_setaffinity");
            }

            gettimeofday(&start, NULL);

            for (int i = 0; i < num_of_calls; i = i + 1){
                write(fd1[1], NULL, 0);
                read(fd2[0], NULL, 0);
            }

            gettimeofday(&finish, NULL);

            // calculate time passed by
            int sec_duration = finish.tv_sec - start.tv_sec;
            int micro_duration = finish.tv_usec - start.tv_usec;
            float average = ((float) (sec_duration * 1000000 + micro_duration)) / num_of_calls;

            // NOTE: somehow this is printed twice
            printf("in average context switching takes %f microseconds long\n", average);

    }
}
