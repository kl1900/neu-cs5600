# Chapter 7
### 1. Compute the response time and turnaround time when running three jobs of length 200 with the SJF and FIFO schedulers. 
SJF:
|  job #     | response time     | turn-around time |
|--------------|-----------|------------|
| job 0 | 0   | 200 |
| job 1 | 200 | 400 |
| job 2 | 400 | 600 |

FIFO:
|  job #     | response time     | turn-around time |
|--------------|-----------|------------|
| job 0 | 0   | 200 |
| job 1 | 200 | 400 |
| job 2 | 400 | 600 |

### 2. Now do the same but with jobs of different lengths: 100, 200, and 300. 

SJF:
|  job #     | response time     | turn-around time |
|--------------|-----------|------------|
| job 0 | 0   | 100 |
| job 1 | 100 | 300 |
| job 2 | 300 | 600 |

FIFO:
|  job #     | response time     | turn-around time |
|--------------|-----------|------------|
| job 0 | 0   | 100 |
| job 1 | 100 | 300 |
| job 2 | 300 | 600 |

### 3. Now do the same, but also with the RR scheduler and a time-slice of 1. 

RR:
|  job #     | response time     | turn-around time |
|--------------|-----------|------------|
| job 0 | 0 | 100 x 3 2 = 298 |
| job 1 | 1 | 100 x 3 + 200 x 2 1 = 499 |
| job 2 | 2 | 100 x 3 + 200 x 2 + 100 x 1 = 600 |


### 4. For what types of workloads does SJF deliver the same turnaround times as FIFO? 

When the jobs are queued from the shortest to longest runtime.


### 5. For what types of workloads and quantum lengths does SJF deliver the same response times as RR? 

When quantum time is the same as the length of each job, and all the jobs have the same length. 

### 6. What happens to response time with SJF as job lengths increase? Can you use the simulator to demonstrate the trend? 

The response time increase.
```sh
python scheduler.py -p SJF -l 100,100,100 -c
python scheduler.py -p SJF -l 200,200,200 -c
python scheduler.py -p SJF -l 300,300,300 -c
```

### 7. What happens to response time with RR as quantum lengths increase? Can you write an equation that gives the worst-case response time, given N jobs?

The response time increase as quantum lengths increase.

Nth job response time = (n-1) * q
Average response time = (0 + 1 + 2 + 3 + ... + n - 1) * q = (n - 1) * q / 2

