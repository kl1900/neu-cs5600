# The Filesystem project

This project implements a simple and straightforward file system.

## Specs:
1. 4096 blocks, of which size 512 bytes each
2. the first 10 blocks are special blocks in the following order:
    - 1 superblock
    - 1 free-block bitmap
    - 1 free-inode bitmap
    - 7 blocks of inodes. Each inode has size 32 bytes. Therefore it has 512 x 7 / 32 = 112 inodes
3. the 11th datablock is the root directory of the file system
4. the rest are free datablock

## features
1. create a file
2. read a file
3. create a new directory
4. read a directory
5. create (inode_idx, filename) entry
6. add entry to a directory

## bugs
1. Only direct datablocks in inodes are supported atm, meaning that file data cannot exceed 10 x 512 = 5120 bytes
2. if one allocated the inode/datablock, but failed to write to it, the inodes/datablock do not flip back to free state
3. the file system does not provide `seek` 
4. the entire filesystem has to be loaded into memory at once
5. no delete/removing files or directories


## How to run
Currently all the code is in [`File.c`](File.c), and the `main()` prints out feature demo.
So just compile it and run.

## TODO
1. support indirect datablock pointing
2. add header files, separate test code and the main code
3. add Make file

