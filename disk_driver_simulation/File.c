#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

#define SOURCE      "./vdisk"
#define BLOCK_SIZE  512
#define NUMBER_OF_BLOCKS 4096
// using the 7 blocks for inodes
#define INODE_SIZE 32
#define NUMBER_OF_INODES (BLOCK_SIZE * 7 / INODE_SIZE)
#define MAGIC_NUMBER 23
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
#define DIR_FILE_TYPE 1
#define FILE_FILE_TYPE 2


typedef struct SuperBlock {
    unsigned int magic_number;
    unsigned int number_of_inodes;
    unsigned int number_of_blocks;
    unsigned int block_size;
    unsigned char other[BLOCK_SIZE - 4 * sizeof(unsigned int)];
} SuperBlock;


typedef struct Bitmap {
    // NUMBER_OF_BLOCKS / 8 = BLOCK_SIZE
    unsigned char map[BLOCK_SIZE];
} Bitmap;


typedef struct Inode {
    unsigned int size;
    int type;
    unsigned short d_blocks[10];
    unsigned short single_indirect_block;
    unsigned short double_indirect_block; 
} Inode;


typedef struct DataBlock {
    char data[BLOCK_SIZE];
} DataBlock;

// create directory block and directory entry
typedef struct DirEntry {
    char inode_idx;
    char filename[31];
} DirEntry;

typedef struct DirBlock {
    DirEntry entries[BLOCK_SIZE / sizeof(DirEntry)];
} DirBlock;


SuperBlock super_block;
Bitmap free_block_v, ibmap;
Inode inodes[NUMBER_OF_INODES];
DataBlock data_blocks[NUMBER_OF_BLOCKS - 10];

int sync_fs() {
    FILE *disk;
    disk = fopen(SOURCE, "w+");
    if (!disk) {
        perror("fopen");
        return EXIT_FAILURE;
    }

    // write superblock
    fwrite(&super_block, sizeof(Bitmap), 1, disk);

    //write free block vector
    fwrite(&free_block_v, sizeof(Bitmap), 1, disk);

    //write inode bitmap
    fwrite(&ibmap, sizeof(Bitmap), 1, disk);

    // write 7 blocks for inodes
    int i;
    for (i = 0; i < NUMBER_OF_INODES; i++) 
    fwrite(&(inodes[i]), sizeof(Inode), 1, disk);

    // write the rest of datablocks
    for (i = 0; i < (NUMBER_OF_BLOCKS - 10); i++)
    fwrite(&(data_blocks[i]), sizeof(DataBlock), 1, disk);

    fclose(disk);
    return 0;
}

int mount_fs() {
    FILE *disk;
    disk = fopen(SOURCE, "r");
    if (!disk) {
        perror("fopen");
        return EXIT_FAILURE;
    }

    // read superblock
    fread(&super_block, sizeof(Bitmap), 1, disk);

    // read free block vector
    fread(&free_block_v, sizeof(Bitmap), 1, disk);

    // read inode bitmap
    fread(&ibmap, sizeof(Bitmap), 1, disk);

    // read 7 blocks of inodes
    fread(&inodes, sizeof(Inode), NUMBER_OF_INODES, disk);

    // read the rest of data blocks
    fread(&data_blocks,sizeof(DataBlock), NUMBER_OF_BLOCKS - 10, disk);

    fclose(disk);
    return 0;
}

int check_avail(Bitmap *bmap, int idx) {
    int off_set = idx + 1;
    int char_idx = off_set / CHAR_BIT;
    int shift = CHAR_BIT - (off_set % CHAR_BIT);
    char c = (*bmap).map[char_idx];
    return (c & ( 1 << shift )) >> shift;
}

// find the first free bit (i.e. 1) at a given start point
int find_free(Bitmap *bmap, int start, unsigned int size) {
    for(int i = start; i < size; i++) {
        if (check_avail(bmap, i) == 1) {
            return i;
        }
    }
    // cannot find a free index, return -1
    return -1;
}

// find the number of 0 or 1 in a bitmap
int get_bitmap_status(Bitmap *bmap, int size, int status) {
    int count = 0;
    for (int idx = 0; idx < size; idx++) {
        if (check_avail(bmap, idx) == status) {
            count ++;
        }
    }
    return count;
}

void toggle_bitmap(Bitmap *bmap, int idx) {
    int off_set = idx + 1;
    int char_idx = off_set / CHAR_BIT;
    int shift = CHAR_BIT - (off_set % CHAR_BIT);
    // use xor to toggle the bit: https://stackoverflow.com/a/19610287
    (*bmap).map[char_idx] ^= (1 << shift);
}

int convert_blk_idx_to_datablk_idx(int idx){
    return idx - 10;
}

int convert_datablk_idx_to_blk_idx(int idx){
    return idx + 10;
}

// find and claim a datablock and return the datablk_idx
int allocate_data_blk() {
    int blk_idx = find_free(&free_block_v, 0, NUMBER_OF_BLOCKS);
    int datablk_idx = convert_blk_idx_to_datablk_idx(blk_idx);
    toggle_bitmap(&free_block_v, blk_idx);
    return datablk_idx;
}

// find and claim a new inode, then return the inode_idx
int allocate_inode() {
    // find new inode
    int inode_idx = find_free(&ibmap, 0, NUMBER_OF_INODES);
    if (inode_idx == -1) {
        printf("no free inode remains\n");
        return EXIT_FAILURE;
    }

    // allocate inode to the new file
    // and claim the new inode
    toggle_bitmap(&ibmap, inode_idx);
    return inode_idx;
}

// write a new file
int num_of_blocks_needed(int size) {
    int temp = size + BLOCK_SIZE -1;
    return temp / BLOCK_SIZE; 
}

// write data to datablocks
int write_file(int inode_idx, char* data, int data_size) {
    // find number of blocks needed
    int num_of_blocks_requested = num_of_blocks_needed(data_size);

    // TODO: file could be just large enough to consume blocks for single_indirect_block
    // thus this check is not sufficient
    if (num_of_blocks_requested > get_bitmap_status(&free_block_v, NUMBER_OF_BLOCKS, 1)) {
        printf("not enough data blocks\n");
        return EXIT_FAILURE;
    }

    // write data
    for(int i = 0; i < num_of_blocks_requested; i++) {
        
        // put first 10 blks in inodes
        if (i < 10) {
            int datablk_idx = allocate_data_blk();
            // break data into block-size chunks and copy data to blocks
            strcpy(data_blocks[datablk_idx].data, data + (i * BLOCK_SIZE));

            // put the blk_idx into the inode
            inodes[inode_idx].d_blocks[i] = convert_datablk_idx_to_blk_idx(datablk_idx);
        }
        // TODO: put blk_idx > 10 in indirect_block references
        
    }
    return 0;
}

// writes data to a datablock, then return the inode
int create_file(char* data, int data_size) {
    int inode_idx = allocate_inode();
    inodes[inode_idx].type = FILE_FILE_TYPE;
    inodes[inode_idx].size = data_size;
    int res = write_file(inode_idx, data, data_size);
    if (res == 0) return inode_idx;
    else return -1;
}


// TODO: enable it to read more than 10 blocks
char *read_file(int inode_idx){
    Inode node = inodes[inode_idx];
    char *data_holder = (char *)malloc(node.size * sizeof(char));
    int off_set = 0;
    for (int i = 0; i < 10; i++) {
        if (node.d_blocks[i] != 0) {
            int datablk_idx = convert_blk_idx_to_datablk_idx(node.d_blocks[i]);
            strcpy(data_holder + (off_set * BLOCK_SIZE), data_blocks[datablk_idx].data);
            off_set++;
        }
    }
    return data_holder;
}

// create a direntry
DirEntry create_direntry(int inode_idx, const char *new_filename, int file_type) {
    DirEntry entry;
    entry.inode_idx = inode_idx;
    inodes[inode_idx].type = file_type;

    // set filename
    if (strlen(new_filename) <= 31) {
        strcpy(entry.filename, new_filename);
    }
    else {
        for (int i = 0; i < 31; i++) {
            entry.filename[i] = *(new_filename + i);
        }
    }
    return entry;
}

void init_dirblk(int datablk_idx, int inode_idx) {
    // convert datablock to dirblock
    DirBlock *dir_blk = (DirBlock *) &(data_blocks[datablk_idx]);
    for (int i = 0; i < (BLOCK_SIZE/sizeof(DirEntry)); i++) {
        DirEntry entry;
        strcpy(entry.filename, "");
        entry.inode_idx = -1;
        dir_blk->entries[i] = entry;
    }

    // associate it with an inode
    inodes[inode_idx].type = DIR_FILE_TYPE;
    inodes[inode_idx].d_blocks[0] = convert_datablk_idx_to_blk_idx(datablk_idx);
    inodes[inode_idx].size = BLOCK_SIZE;
}

// add a directory entry to a directory block
int add_entry_to_dir(int datablk_idx, DirEntry entry) {
    DirBlock *dir_blk = (DirBlock *) &(data_blocks[datablk_idx]);
    for (int i = 0; i < (BLOCK_SIZE/sizeof(DirEntry)); i++) {
        if (dir_blk->entries[i].inode_idx == -1) {
            dir_blk->entries[i] = entry;
            return 0;
        }
    }
    return -1;
}

void read_dir(int inode_idx){
    if (inodes[inode_idx].type != DIR_FILE_TYPE) {
        printf("ERROR: inode does not correspond to a directory");
    }
    // TODO: enable dir to read more than 10 blks
    for (int i = 0; i < 10; i++) {
        int blk_idx = inodes[inode_idx].d_blocks[i];
        if (blk_idx != 0) {
            int datablk_idx = convert_blk_idx_to_datablk_idx(blk_idx);
            DirBlock *dir_blk = (DirBlock *) &(data_blocks[datablk_idx]);
            for (int j = 0; j < (BLOCK_SIZE/sizeof(DirEntry)); j++ ) {
                int f_inode_idx = dir_blk->entries[j].inode_idx;
                if (f_inode_idx != -1) {
                    if (inodes[f_inode_idx].type == DIR_FILE_TYPE) {
                        printf("inode %d has dir: ",dir_blk->entries[j].inode_idx);
                    } else if (inodes[f_inode_idx].type == FILE_FILE_TYPE){
                        printf("inode %d has file: ",dir_blk->entries[j].inode_idx);
                    }
                    printf("%s\n", dir_blk->entries[j].filename);
                }
            }
        }
    }
}

// initialize a new file system
int initialize_fs() {
   
    int i;
    // initialize superblock
    super_block.magic_number = MAGIC_NUMBER;
    super_block.number_of_inodes = NUMBER_OF_INODES;
    super_block.number_of_blocks = NUMBER_OF_BLOCKS;
    super_block.block_size = BLOCK_SIZE * sizeof(char);
    for (i = 0; i < (BLOCK_SIZE - 4 * sizeof(unsigned int)); i++) {
        super_block.other[i] = 0;
    }

    // initialize free block vector bitmap
    free_block_v.map[0] = 0;
    free_block_v.map[1] = 0b00111111;
    for (i = 2; i < BLOCK_SIZE; i++) free_block_v.map[i] = 255;

    // initialize inode bitmap
    for (i = 0; i < BLOCK_SIZE; i++) ibmap.map[i] = 255;
    

    // initialize empty inodes
    for (i = 0; i < NUMBER_OF_INODES; i++) {
        inodes[i].size = 0;
        inodes[i].type = -1;
        memset(inodes[i].d_blocks, 0, 10);
        inodes[i].single_indirect_block = 0;
        inodes[i].double_indirect_block = 0;
    }

    // initialize the rest of datablocks
    for (i = 0; i < (NUMBER_OF_BLOCKS - 10); i++) {
        memset(data_blocks[i].data, 0, BLOCK_SIZE);
    }


    // init the root directory
    int dir_inode_idx = allocate_inode();
    int datablk_idx = allocate_data_blk();
    if (datablk_idx < 0) return EXIT_FAILURE;
    init_dirblk(datablk_idx, dir_inode_idx);
    
    DirEntry root_entry = create_direntry(dir_inode_idx, "root", DIR_FILE_TYPE);
    int res = add_entry_to_dir(datablk_idx, root_entry);
    if (res < 0) return EXIT_FAILURE;

    return EXIT_SUCCESS;
}



int main() {
    initialize_fs();
    // read_dir(0);
    int dir_inode_idx = allocate_inode();
    int datablk_idx = allocate_data_blk();
    init_dirblk(datablk_idx, dir_inode_idx);
    
    // creating new dir abc
    DirEntry new_entry = create_direntry(dir_inode_idx, "abc", DIR_FILE_TYPE);
    int res = add_entry_to_dir(0, new_entry);


    // printf("====== Now adding a sample.txt file =========\n");
    // write a small file
    char *new_data = "Sleeping in his car was never the plan but sometimes things don't work out as planned. This had been his life for the last three months and he was just beginning to get used to it. He didn't actually enjoy it, but he had accepted it and come to terms with it. Or at least he thought he had. All that changed when he put the key into the ignition, turned it and the engine didn't make a sound. Sleeping in his car was never the plan but sometimes things don't work out as planned. This had been his life for the last three months and he was just beginning to get used to it. He didn't actually enjoy it, but he had accepted it and come to terms with it. Or at least he thought he had. All that changed when he put the key into the ignition, turned it and the engine didn't make a sound.";
    int len = strlen(new_data);
    int f_inode_idx = create_file(new_data, len);
    DirEntry f_entry = create_direntry(f_inode_idx, "sample.txt\n", FILE_FILE_TYPE);
    add_entry_to_dir(dir_inode_idx, f_entry);
    // sync_fs();
    // // read the new data
    // mount_fs();
    char *read_data = read_file(f_inode_idx);
    printf("dir root:\n");
    read_dir(0);
    printf("dir abc:\n");
    read_dir(dir_inode_idx);
    printf("reading data from inode %d: %s\n",f_inode_idx, read_data);
    
	return EXIT_SUCCESS;
}
