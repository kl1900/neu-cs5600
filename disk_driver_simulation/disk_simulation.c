// Disk parameters

//     Size of a block: 512 bytes
//     Number of blocks on disk: 4096
//     Name of file simulating disk: “vdisk” in current directory
//     Blocks are numbered from 0 to 4095

 

// Block 0 – superblock

//     Contains information about the filesystem implementation, such as
//     first 4 bytes: magic number to identify the filesystem the disk is formatted for
//     next 4 bytes: number of blocks on disk
//     next 4 bytes: number of inodes for disk (more on this soon, for now us 42!)

 

// Block 1 – free block vector

//     With 512 bytes in this block, and 8 bits per byte, our free-block vector may hold 4096 bits.
//     First ten blocks (0 through 9) are not available for data, more on this soon.
//     To indicate an available block, bit must be set to 1.

 

// Other Blocks

//     You can reserve other blocks for other persistent data structures in your filesystem
//     The remaining blocks will be for files in your filesystem! :)
// nice tutorial on lseek: https://www.youtube.com/watch?v=_-OFPZVVYzg

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SOURCE      "./vdisk"
#define BLOCK_SIZE  512
#define NUMBER_OF_BLOCKS 4096
#define NUMBER_OF_INODES 42
#define DISK_SIZE   (NUMBER_OF_BLOCKS * BLOCK_SIZE)
#define MAGIC_NUMBER 3
#define GET_BLOCK(file, n) (fseek(file, n * BLOCK_SIZE, SEEK_SET))
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

// file disk
FILE *disk;

typedef struct Block {
    int data[BLOCK_SIZE / 4];
} Block;

int *read_block(int idx) {
    GET_BLOCK(disk, idx);
    int buffer[BLOCK_SIZE / 4];
    int *data = (int *)malloc(128 * sizeof(int));
    size_t ret = fread(buffer, sizeof(*buffer), ARRAY_SIZE(buffer), disk);
    if (ret != ARRAY_SIZE(buffer)) {
        fprintf(stderr, "fread() failed: %zu\n", ret);
        exit(EXIT_FAILURE);
    }
    data = buffer;
    return data;
}

int write_block(int *data, int idx) {
    GET_BLOCK(disk, idx);
    Block b;
    for (int i = 0; i < sizeof(data); i++) b.data[i] = data[i];
    size_t ret = fwrite(&b, 1, sizeof(b), disk);
    return 0;
}

int init_disk(int debug) {
    // put meta info in superblock 0

    int b0[3] = {MAGIC_NUMBER, NUMBER_OF_BLOCKS, NUMBER_OF_INODES};
    write_block(b0, 0);

    // set block 1 ,aka the free block vector
    // To indicate an available block, bit must be set to 1
    // First ten blocks (0 through 9) are not available for data

    GET_BLOCK(disk, 1);
    Block b1;
    // set first 10 bits to 0
    b1.data[0] = 0xffc0;
    fwrite(&b1, 1, sizeof(b1), disk);

    if (debug == 1) {
        // read superblock
        GET_BLOCK(disk, 0);
        int a,b,c;
        fread(&a, 1,sizeof(int), disk);
        fread(&b, 1, sizeof(int), disk);
        fread(&c, 1, sizeof(int),disk);
        printf("magic number is %d\n", a);
        printf("number of blocks is %d\n",b);
        printf("number of inodes is %d\n",c);
        // read block 1
        GET_BLOCK(disk, 1);
        fread(&a, 1, sizeof(int),disk);
        printf("block 1 data is %x\n", a);
    }
    return 0;
}

int main() {
    disk = fopen(SOURCE, "w+");
    if (!disk) {
        perror("fopen");
        return EXIT_FAILURE;
    }

	init_disk(1);
    int *buffer = read_block(1);
    printf("buffer reads block 0 is %d\n",*buffer);


	fclose(disk);
    free(buffer);
	return EXIT_SUCCESS;
}
