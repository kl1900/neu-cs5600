## where are you storing inode and data, 
1. 4096 blocks, of which size 512 bytes each
2. the first 10 blocks are special blocks in the following order:
    - 1 superblock
    - 1 free-block bitmap
    - 1 free-inode bitmap
    - 7 blocks of inodes. Each inode has size 32 bytes. So in total of 112 inodes


Each inode is defined as:
- unsigned int size;
- int type;
- unsigned short d_blocks[10];
- unsigned short single_indirect_block;
- unsigned short double_indirect_block; 

A free datablock can either be a directory block, or a file block.
Directory block is defined as pairs of (inode_idx, filename).
File block is 512 bytes of data.

## how are you keeping track of free inodes and data blocks
I have a bitmap for inodes and datablocks each. When calling `allocate_inode` and `allocate_data_blk()`, it finds the first free bit and its index, toggle the bit to claim the node/datablock, the returns the index of the inode/datablock.

## what is content of super block
1. MAGIC_NUMBER
2. NUMBER_OF_INODES
3. NUMBER_OF_BLOCKS
4. BLOCK_SIZE

## what assumptions have you made
1. performance is not an issue
2. memory is big enough so the entire filesystem can be loaded into memory at once
