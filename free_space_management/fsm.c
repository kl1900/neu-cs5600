// this code is based on concept from this lecture:
// https://my.eng.utah.edu/~cs4400/malloc-1.html

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

// define chunk sizes
#define ALIGNMENT 16
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~(ALIGNMENT-1))
#define CHUNK_SIZE (1 << 14)
#define CHUNK_ALIGN(size) (((size)+(CHUNK_SIZE-1)) & ~(CHUNK_SIZE-1))

// find current block header's info
#define OVERHEAD (sizeof(block_header) + sizeof(block_footer))
#define GET_SIZE(p) ((block_header *)(p)) -> size
#define GET_ALLOC(p) ((block_header *)(p)) -> allocated

// find current block's header and footer using bp
#define HDRP(bp) ((char *)(bp) - sizeof(block_header))
#define FTRP(bp) ((char *)(bp) + GET_SIZE(HDRP(bp))-OVERHEAD)

// find next or previous block using bp
#define NEXT_BLKP(bp) ((char *)(bp) + GET_SIZE(HDRP(bp)))
#define PREV_BLKP(bp) ((char *)(bp) - GET_SIZE((char *)(bp)-OVERHEAD))

typedef struct {
    size_t size;
    char allocated;
} block_header;

typedef struct {
    size_t size;
    char filler;
} block_footer;


void *first_bp;

void extend(size_t size) {
    size_t chunk_size = CHUNK_ALIGN(size);
    void *bp = sbrk(chunk_size);

    // set current block size
    GET_SIZE(HDRP(bp)) = chunk_size;
    GET_SIZE(FTRP(bp)) = chunk_size;
    GET_ALLOC(HDRP(bp)) = 0;

    // set the next bp to 0
    GET_SIZE(NEXT_BLKP(bp)) = 0;
    GET_ALLOC(NEXT_BLKP(bp)) = 1;
}


void set_allocated(void *bp, size_t size) {
    // can only use the space without header
    size_t extra_size = GET_SIZE(HDRP(bp)) - size;
    
    // if there is extra space, split the block
    // we cannot use 1 block, therefore leave it empty
    // if extra size is 1 block
    if (extra_size > ALIGN(1 + OVERHEAD)) {
        GET_SIZE(HDRP(bp)) = size;
        GET_SIZE(FTRP(bp)) = size;

        // split the next block
        GET_SIZE(HDRP(NEXT_BLKP(bp))) = extra_size;
        GET_SIZE(FTRP(NEXT_BLKP(bp))) = extra_size;
        GET_ALLOC(HDRP(NEXT_BLKP(bp))) = 0;
    }
    
    GET_ALLOC(HDRP(bp)) = 1;
}


void *my_malloc(size_t size) {
    // needs add header to it
    int req_size = ALIGN(size + OVERHEAD);

    void *bp = first_bp;

    while(GET_SIZE(HDRP(bp)) != 0) {
        if ( !GET_ALLOC(HDRP(bp)) && (GET_SIZE(HDRP(bp)) >= req_size) ) {
            set_allocated(bp, req_size);
            return bp;
        }
        bp = NEXT_BLKP(bp);
    }
    // if no room is found, need to extend the memory space
    extend(req_size);
    set_allocated(bp, req_size);
    return bp;

}


void *coalesce(void *bp) {
    size_t prev_alloc = GET_ALLOC(HDRP(PREV_BLKP(bp)));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));   
    size_t size = GET_SIZE(HDRP(bp));
    
    // if both prev block and next block are occupied, do nothing
    if (prev_alloc && next_alloc) {
        // do nothing
    }
    // if next block is free, merge current into the next block
    else if (prev_alloc && !next_alloc) {
        size += GET_SIZE(HDRP(NEXT_BLKP(bp)));
        GET_SIZE(HDRP(bp)) = size;
        GET_SIZE(FTRP(bp)) = size;
    }
    // if previous block is free, merge into previous block
    else if (!prev_alloc && next_alloc) {
        size += GET_SIZE(HDRP(PREV_BLKP(bp)));
        GET_SIZE(FTRP(bp)) = size;
        GET_SIZE(HDRP(PREV_BLKP(bp))) = size;
        bp = PREV_BLKP(bp);
    }
    // if both sides are free, combine all as a big free block
    else {
        size += (GET_SIZE(HDRP(PREV_BLKP(bp)))
             + GET_SIZE(HDRP(NEXT_BLKP(bp))));
        GET_SIZE(HDRP(PREV_BLKP(bp))) = size;
        GET_SIZE(FTRP(NEXT_BLKP(bp))) = size;
        bp = PREV_BLKP(bp);
    }

    return bp;
}

void my_free(void *bp) {
    GET_ALLOC(HDRP(bp)) = 0;
    coalesce(bp);
} 

int mm_init() {
    sbrk(sizeof(block_header));
    first_bp = sbrk(0);

    GET_SIZE(HDRP(first_bp)) = 0;
    GET_ALLOC(HDRP(first_bp)) = 1;

    // set a block so that 1st free doesn't go non-existense block
    my_malloc(0);

    return 0;
}

int main() {
    mm_init();

    // make sure my_malloc works
    int *x1 = (int *) my_malloc(3 * sizeof(int));
    x1[0] = 1;
    x1[1] = 2;
    x1[2] = 3;
    for (int i = 0; i < 3; i++) {
        printf("x1 element %d is %d\n", i, x1[i]);
    }
    
    // check if free'd memory is reused
    my_free(x1);
    int *x2 = (int *) my_malloc(2 * sizeof(int));

    
    int *x5 = (int *) my_malloc(3 * sizeof(int));
    x5[0] = 7;
    x5[1] = 8;
    x5[2] = 9;
    for (int i = 0; i < 3; i++) {
        printf("x5 element %d is %d\n", i, x5[i]);
    }
    my_free(x2);
    my_free(x5);

    return 0;
}



