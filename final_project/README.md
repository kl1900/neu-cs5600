# Final Project
This is the final project for NEU CS5600

The objective of this project implements and compares different strategies in allocating
appropriate memory in free space, including:
1. best fit
2. worst fit
3. first fit
4. next fit

descriptions of these strategies can be found in [OSTEP chapter 12](https://pages.cs.wisc.edu/~remzi/OSTEP/vm-freespace.pdf)

## The structure
Each "node" of the memory contains:
- a header, including its node size and if it is allocated
- the allocated memory
- a footer, including the same info as header, to be able to refer nodes backwards

When the memory space is initialized, it creates a 0-byte node to mark its beginning.

# Measuring the performance of different strategies
It is obvious that _first fit_ and _next fit_ are going to find node faster than _best fit_ and _worst fit_, because they do not need to search through the entire free-memory node list. So the metrics I want to measure is how fragmented it is after 3000 random memory requests.

To do so, I first generate random memory requests ranging from 1x to 100x ints, and free
a node every 4 malloc.

Then all we need is to see how much total memory each strategy requests.

# The result

```
kuolu@lemur-pro:final_project$ ./"project_fsm" 
== first fit ===
Total nodes: 1501
Total memory used: 376832
== next fit ===
Total nodes: 1502
Total memory used: 409600
== best fit ===
Total nodes: 1501
Total memory used: 376832
== worst fit ===
Total nodes: 1501
Total memory used: 376832
```

after compiling and running the program, we see that the _next fit_ uses most memory.
And all other methods uses the same amount of memory.

I think this is due to the part that I "align" the requested size to bigger chunks, and I coalesce the nodes as I free them.

Without coalescing the freed nodes, I have
```
== first fit ===
Total nodes: 1511
Total memory used: 376908
== next fit ===
Total nodes: 1512
Total memory used: 393216
== best fit ===
Total nodes: 1506
Total memory used: 376832
== worst fit ===
Total nodes: 1506
Total memory used: 376832
```

This shows that _best / worst fit_ requires least amount of memory, while _first fit_ does
require more memory, but is not as bad as _next fit_
