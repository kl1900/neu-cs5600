// this code is based on concept from this lecture:
// https://my.eng.utah.edu/~cs4400/malloc-1.html

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>


// define chunk sizes
#define ALIGNMENT 16
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~(ALIGNMENT-1))
#define CHUNK_SIZE (1 << 14)
#define CHUNK_ALIGN(size) (((size)+(CHUNK_SIZE-1)) & ~(CHUNK_SIZE-1))

// find current block header's info
#define OVERHEAD (sizeof(block_header) + sizeof(block_footer))
#define GET_SIZE(p) ((block_header *)(p)) -> size
#define GET_ALLOC(p) ((block_header *)(p)) -> allocated

// find current block's header and footer using bp
#define HDRP(bp) ((char *)(bp) - sizeof(block_header))
#define FTRP(bp) ((char *)(bp) + GET_SIZE(HDRP(bp))-OVERHEAD)

// find next or previous block using bp
#define NEXT_BLKP(bp) ((char *)(bp) + GET_SIZE(HDRP(bp)))
#define PREV_BLKP(bp) ((char *)(bp) - GET_SIZE((char *)(bp)-OVERHEAD))

typedef struct {
    size_t size;
    char allocated;
} block_header;

typedef struct {
    size_t size;
    char filler;
} block_footer;

// this returns the search result of various strategies
typedef struct {
    char found;
    void *bp;
} search_result;

/* Function declarations */
search_result first_fit(size_t req_size);
search_result next_fit(size_t req_size);
search_result best_fit(size_t req_size);
search_result worst_fit(size_t req_size);


// global variable
void *first_bp;
search_result (* strategy)(size_t req_size);

// different strategies
search_result first_fit(size_t req_size) {
    search_result result;
    void *bp = first_bp;
    while(GET_SIZE(HDRP(bp)) != 0) {
        if ( !GET_ALLOC(HDRP(bp)) && (GET_SIZE(HDRP(bp)) >= req_size) ) {
            result.found = 1;
            result.bp = bp;
            return result;
        }
        bp = NEXT_BLKP(bp);
    }

    // not finding the appropriate block
    result.found = 0;
    result.bp = bp;
    return result;
}

search_result next_fit(size_t req_size){
    search_result result;
    void *bp = first_bp;
    char first_fit = 0;
    while(GET_SIZE(HDRP(bp)) != 0) {
        if ( !GET_ALLOC(HDRP(bp)) && (GET_SIZE(HDRP(bp)) >= req_size) ) {
            if (!first_fit) {
                first_fit = 1;
            } else {
                result.found = 1;
                result.bp = bp;
                return result;
            }
        }
        bp = NEXT_BLKP(bp);
    }

    // not finding the appropriate block
    result.found = 0;
    result.bp = bp;
    return result;

}

search_result best_fit(size_t req_size){
    search_result result;
    void *bp = first_bp;
    void *best_fit_bp = NULL;
    while(GET_SIZE(HDRP(bp)) != 0) {
        if ( !GET_ALLOC(HDRP(bp)) && (GET_SIZE(HDRP(bp)) >= req_size) ) {
            if ((best_fit_bp == NULL) || 
                (GET_SIZE(HDRP(bp)) < GET_SIZE(HDRP(best_fit_bp)))) 
                {
                    best_fit_bp = bp;
                }
        }
        bp = NEXT_BLKP(bp);
    }

    // not finding the appropriate block
    if (best_fit_bp == NULL) {
        result.found = 0;
        result.bp = bp;
    } else {
        result.found = 1;
        result.bp = best_fit_bp;
    }
    
    return result;
}


search_result worst_fit(size_t req_size){
    search_result result;
    void *bp = first_bp;
    void *worst_fit_bp = NULL;
    while(GET_SIZE(HDRP(bp)) != 0) {
        if ( !GET_ALLOC(HDRP(bp)) && (GET_SIZE(HDRP(bp)) >= req_size) ) {
            if ((worst_fit_bp == NULL) || 
                (GET_SIZE(HDRP(bp)) > GET_SIZE(HDRP(worst_fit_bp)))) 
                {
                    worst_fit_bp = bp;
                }
        }
        bp = NEXT_BLKP(bp);
    }
    // not finding the appropriate block
    if (worst_fit_bp == NULL) {
        result.found = 0;
        result.bp = bp;
    } else {
        result.found = 1;
        result.bp = worst_fit_bp;
    }
    return result;
}


// the main program
void extend(size_t size) {
    size_t chunk_size = CHUNK_ALIGN(size);
    void *bp = sbrk(chunk_size);

    // set current block size
    GET_SIZE(HDRP(bp)) = chunk_size;
    GET_SIZE(FTRP(bp)) = chunk_size;
    GET_ALLOC(HDRP(bp)) = 0;

    // set the next bp to 0
    GET_SIZE(NEXT_BLKP(bp)) = 0;
    GET_ALLOC(NEXT_BLKP(bp)) = 1;
}


void set_allocated(void *bp, size_t size) {
    // can only use the space without header
    size_t extra_size = GET_SIZE(HDRP(bp)) - size;
    
    // if there is extra space, split the block
    // we cannot use 1 block, therefore leave it empty
    // if extra size is 1 block
    if (extra_size > ALIGN(1 + OVERHEAD)) {
        GET_SIZE(HDRP(bp)) = size;
        GET_SIZE(FTRP(bp)) = size;

        // split the next block
        GET_SIZE(HDRP(NEXT_BLKP(bp))) = extra_size;
        GET_SIZE(FTRP(NEXT_BLKP(bp))) = extra_size;
        GET_ALLOC(HDRP(NEXT_BLKP(bp))) = 0;
    }
    
    GET_ALLOC(HDRP(bp)) = 1;
}


void *my_malloc(size_t size) {
    // needs add header to it
    int req_size = ALIGN(size + OVERHEAD);

    search_result result= strategy(req_size);

    // if no room is found, need to extend the memory space
    if (result.found == 0) {
        extend(req_size);
    }
    set_allocated(result.bp, req_size);
    return result.bp;
}


void *coalesce(void *bp) {
    size_t prev_alloc = GET_ALLOC(HDRP(PREV_BLKP(bp)));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));   
    size_t size = GET_SIZE(HDRP(bp));
    
    // if both prev block and next block are occupied, do nothing
    if (prev_alloc && next_alloc) {
        // do nothing
    }
    // if next block is free, merge current into the next block
    else if (prev_alloc && !next_alloc) {
        size += GET_SIZE(HDRP(NEXT_BLKP(bp)));
        GET_SIZE(HDRP(bp)) = size;
        GET_SIZE(FTRP(bp)) = size;
    }
    // if previous block is free, merge into previous block
    else if (!prev_alloc && next_alloc) {
        size += GET_SIZE(HDRP(PREV_BLKP(bp)));
        GET_SIZE(FTRP(bp)) = size;
        GET_SIZE(HDRP(PREV_BLKP(bp))) = size;
        bp = PREV_BLKP(bp);
    }
    // if both sides are free, combine all as a big free block
    else {
        size += (GET_SIZE(HDRP(PREV_BLKP(bp)))
             + GET_SIZE(HDRP(NEXT_BLKP(bp))));
        GET_SIZE(HDRP(PREV_BLKP(bp))) = size;
        GET_SIZE(FTRP(NEXT_BLKP(bp))) = size;
        bp = PREV_BLKP(bp);
    }

    return bp;
}

void my_free(void *bp) {
    GET_ALLOC(HDRP(bp)) = 0;
    // coalesce(bp);
} 

void free_all() {
    void *bp = first_bp;
    while(GET_SIZE(HDRP(bp)) != 0) {
        if (GET_ALLOC(HDRP(bp))) {
            my_free(bp);
        }
        bp = NEXT_BLKP(bp);
    }
}

int mm_init() {
    sbrk(sizeof(block_header));
    first_bp = sbrk(0);

    GET_SIZE(HDRP(first_bp)) = 0;
    GET_ALLOC(HDRP(first_bp)) = 1;

    // set a block so that 1st free doesn't go non-existense block
    my_malloc(0);
    return 0;
}

int count_nodes() {
    int counter = 0;
    void *bp = first_bp;
    while(GET_SIZE(HDRP(bp)) != 0) {
        // if (GET_ALLOC(HDRP(bp))) {
            counter ++;
        // }
        bp = NEXT_BLKP(bp);
    }
    // uncount the head and foot node of the memory space
    return counter - 2;
}

int count_mem() {
    int counter = 0;
    void *bp = first_bp;
    while(GET_SIZE(HDRP(bp)) != 0) {
        counter += GET_SIZE(HDRP(bp));
        bp = NEXT_BLKP(bp);
    }
    // uncount the head and foot node of the memory space
    return counter - GET_SIZE(HDRP(bp));
}

void print_status() {
    printf("Total nodes: %d\n", count_nodes());
    printf("Total memory used: %d\n", count_mem());
}


void do_test(int test[], int test_size) {
    mm_init();
    void *to_free;
    int counter = 0;
    int div = 4;
    for (int i = 0; i < test_size; i++) {
        if (counter % div == 0) to_free = my_malloc(test[i] * sizeof(int));
        else if (counter % div == 1) my_free(to_free);
        else my_malloc(test[i] * sizeof(int));
        counter ++;
    }
}


int main() {
    int test_size = 3000;
    int upper = 100;
    int lower = 1;

    int test[test_size];
    for (int i = 0; i < test_size; i++) {
        int num = (rand() %
           (upper - lower + 1)) + lower;
        test[i] = num;
    }

    // test first_fit
    strategy = first_fit;
    do_test(test, test_size);
    printf("== first fit ===\n");
    print_status();
    
    // test next_fit
    strategy = next_fit;
    do_test(test, test_size);
    printf("== next fit ===\n");
    print_status();

    // test best_fit
    strategy = best_fit;
    do_test(test, test_size);
    printf("== best fit ===\n");
    print_status();

    // test worst_fit
    strategy = best_fit;
    do_test(test, test_size);
    printf("== worst fit ===\n");
    print_status();

    return 0;
}

