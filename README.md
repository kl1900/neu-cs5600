# Northeastern University CS5600 Homework Repo
This repo is to maintain HW assignments for NEU CS5600 class. 

The class follows the textbook [Operating Systems: Three Easy Pieces](https://pages.cs.wisc.edu/~remzi/OSTEP/) (OSTEP), and most of the HW are my answers to its [homework assignments](https://pages.cs.wisc.edu/~remzi/OSTEP/Homework/homework.html).

Each answers to homework questions are stored in `answers.md` for each chapter.

Note: some python 2 code from the textbook homeworks have been converted to python 3 using `2to3` python module. And its python 2 version are stored as `<program_name>.bak` in the same directory.
