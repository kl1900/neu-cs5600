import os
import re
import subprocess
import numpy as np
import matplotlib.pyplot as plt

# Figure 9.2 plots the average fairness as the length of the two jobs (R)
# is varied from 1 to 1000 over thirty trials (results are generated via the
# simulator provided at the end of the chapter)

PROGRAM_PATH = os.path.join("..","ostep-homework","cpu-sched-lottery","lottery.py")
NUM_OF_TRIALS = 30
TICKETS = 100

def find_done_time(input):
    p = r"--> JOB.+DONE at time (?P<time>[0-9]{0,3})"
    m = re.search(p, input)
    if m is not None:
        return m.group("time")
    else:
        print(input)

data = list()
for R in range(1,1001):
    s = 0.0
    for i in range(NUM_OF_TRIALS):
        job_list = f"{R}:{TICKETS},{R}:{TICKETS}"
        command = ["python", PROGRAM_PATH, "-s", f"{i}", "-l",job_list, "-c"]
        output = subprocess.run(command, capture_output=True, text=True)
        t = find_done_time(output.stdout)
        s += float(t) / (2 * R)
    data.append(s)

fig = plt.figure()
x = np.linspace(1, 1000, 1000)
plt.plot(x, [d / NUM_OF_TRIALS for d in data])
plt.xlabel("Job Length")
plt.ylabel("Fairness")
plt.title("Figure 9.2: Lottery Fairness Study")
plt.savefig("plot_random_sched.png")
plt.show()
