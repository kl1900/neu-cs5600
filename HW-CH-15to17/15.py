import random

import matplotlib.pyplot as plt
import numpy as np

adress_space = 1024
physical_memory = 3 * 1024
y = [0] * physical_memory
number_of_seeds = 100


for i in range(number_of_seeds):
    random.seed(i)
    for j in range(physical_memory):
        limit = j
        virtual_address = int(adress_space * random.random())
        if (virtual_address < limit):
            y[j] += 1

fig = plt.figure()
x = np.linspace(1, physical_memory, physical_memory)
plt.plot(x, [i / number_of_seeds for i in y])
plt.ylim(0, 1)
plt.margins(0)
plt.xlabel('Limit')
plt.ylabel('Valid Addresses (Average)')
plt.savefig('ch15_q5.png', dpi=227)
