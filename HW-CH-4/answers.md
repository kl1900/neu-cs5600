[[_TOC_]]
# Chapter 4 Questions 
### 1. Run process-run.py with the following flags: `-l 5:100,5:100`. What should the CPU utilization be (e.g., the percent of time the CPU is in use?) Why do you know this? Use the `-c` and `-p` flags to see if you were right.

The CPU should be in use 100% of the time, because it is specified in the parameter `100` for both processes. And the printout shows that I am correct:
```
python process-run.py -l 5:100,5:100 -c -p
Time     PID: 0     PID: 1        CPU        IOs 
  1     RUN:cpu      READY          1            
  2     RUN:cpu      READY          1            
  3     RUN:cpu      READY          1            
  4     RUN:cpu      READY          1            
  5     RUN:cpu      READY          1            
  6        DONE    RUN:cpu          1            
  7        DONE    RUN:cpu          1            
  8        DONE    RUN:cpu          1            
  9        DONE    RUN:cpu          1            
 10        DONE    RUN:cpu          1            

Stats: Total Time 10
Stats: CPU Busy 10 (100.00%)
Stats: IO Busy  0 (0.00%)
```

### 2. Now run with these flags: `./process-run.py -l 4:100,1:0`. These flags specify one process with 4 instructions (all to use the CPU), and one that simply issues an I/O and waits for it to be done. How long does it take to complete both processes? Use  `-c` and `-p` to find out if you were right.  

The total time should be 4 CPU ticks + 5 IO ticks = 9 ticks. However, looks like I was wrong:
```
python process-run.py -l 4:100,1:0 -c -p
Time     PID: 0     PID: 1        CPU        IOs 
  1     RUN:cpu      READY          1            
  2     RUN:cpu      READY          1            
  3     RUN:cpu      READY          1            
  4     RUN:cpu      READY          1            
  5        DONE     RUN:io          1            
  6        DONE    WAITING                     1 
  7        DONE    WAITING                     1 
  8        DONE    WAITING                     1 
  9        DONE    WAITING                     1 
 10*       DONE       DONE                       

Stats: Total Time 10
Stats: CPU Busy 5 (50.00%)
Stats: IO Busy  4 (40.00%)
```
The IO ticks has 1 tick for `DONE` to complete an IO task. So the total time is $`4+5+1=10`$

### 3. Switch the order of the processes: `-l 1:0,4:100`. What happens now? Does switching the order matter? Why? (As always, use  `-c` and `-p` to see if you were right)

Switching the order allows the 2nd process to utilize the CPU while the 1st process is running IO, and thus use both resources efficiently. Therefore switching the order of the processes matter.

```
python process-run.py -l 1:0,4:100 -c -p
Time     PID: 0     PID: 1        CPU        IOs 
  1      RUN:io      READY          1            
  2     WAITING    RUN:cpu          1          1 
  3     WAITING    RUN:cpu          1          1 
  4     WAITING    RUN:cpu          1          1 
  5     WAITING    RUN:cpu          1          1 
  6*       DONE       DONE                       

Stats: Total Time 6
Stats: CPU Busy 5 (83.33%)
Stats: IO Busy  4 (66.67%)
```

### 4. We’ll now explore some of the other flags. One important flag is `-S`, which determines how the system reacts when a process issues an I/O. With the flag set to `SWITCH_ON_END`, the system will NOT switch to another process while one is doing I/O, instead waiting until the process is completely finished. What happens when you run the following two processes (`-l 1:0,4:100 -c -S SWITCH_ON_END`), one doing I/O and the other doing CPU work?  

```
python process-run.py -l 1:0,4:100 -c -S SWITCH_ON_END
Time     PID: 0     PID: 1        CPU        IOs 
  1      RUN:io      READY          1            
  2     WAITING      READY                     1 
  3     WAITING      READY                     1 
  4     WAITING      READY                     1 
  5     WAITING      READY                     1 
  6*       DONE    RUN:cpu          1            
  7        DONE    RUN:cpu          1            
  8        DONE    RUN:cpu          1            
  9        DONE    RUN:cpu          1            
```
### 5. Now, run the same processes, but with the switching behavior set to switch to another process whenever one is WAITING for I/O (`-l 1:0,4:100 -c -S SWITCH_ON_IO`). What happens now? Use  `-c` and `-p` to confirm that you are right.

The system switches to another process while one is doing I/O. And the output shows that I am correct:
```
python process-run.py -l 1:0,4:100 -c -S SWITCH_ON_IO -p
Time     PID: 0     PID: 1        CPU        IOs 
  1      RUN:io      READY          1            
  2     WAITING    RUN:cpu          1          1 
  3     WAITING    RUN:cpu          1          1 
  4     WAITING    RUN:cpu          1          1 
  5     WAITING    RUN:cpu          1          1 
  6*       DONE       DONE                       

Stats: Total Time 6
Stats: CPU Busy 5 (83.33%)
Stats: IO Busy  4 (66.67%)
```

### 6. One other important behavior is what to do when an I/O completes. With `-I IO_RUN_LATER`, when an I/O completes, the process that issued it is not necessarily run right away; rather, whatever was running at the time keeps running. What happens when you run this combination of processes? (Run `./process-run.py -l 3:0,5:100,5:100,5:100 -S SWITCH_ON_IO -I IO_RUN_LATER -c -p`) Are system resources being effectively utilized?

```
python process-run.py -l 3:0,5:100,5:100,5:100 -S SWITCH_ON_IO -I IO_RUN_LATER -c -p
Time     PID: 0     PID: 1     PID: 2     PID: 3        CPU        IOs 
  1      RUN:io      READY      READY      READY          1            
  2     WAITING    RUN:cpu      READY      READY          1          1 
  3     WAITING    RUN:cpu      READY      READY          1          1 
  4     WAITING    RUN:cpu      READY      READY          1          1 
  5     WAITING    RUN:cpu      READY      READY          1          1 
  6*      READY    RUN:cpu      READY      READY          1            
  7       READY       DONE    RUN:cpu      READY          1            
  8       READY       DONE    RUN:cpu      READY          1            
  9       READY       DONE    RUN:cpu      READY          1            
 10       READY       DONE    RUN:cpu      READY          1            
 11       READY       DONE    RUN:cpu      READY          1            
 12       READY       DONE       DONE    RUN:cpu          1            
 13       READY       DONE       DONE    RUN:cpu          1            
 14       READY       DONE       DONE    RUN:cpu          1            
 15       READY       DONE       DONE    RUN:cpu          1            
 16       READY       DONE       DONE    RUN:cpu          1            
 17      RUN:io       DONE       DONE       DONE          1            
 18     WAITING       DONE       DONE       DONE                     1 
 19     WAITING       DONE       DONE       DONE                     1 
 20     WAITING       DONE       DONE       DONE                     1 
 21     WAITING       DONE       DONE       DONE                     1 
 22*     RUN:io       DONE       DONE       DONE          1            
 23     WAITING       DONE       DONE       DONE                     1 
 24     WAITING       DONE       DONE       DONE                     1 
 25     WAITING       DONE       DONE       DONE                     1 
 26     WAITING       DONE       DONE       DONE                     1 
 27*       DONE       DONE       DONE       DONE                       

Stats: Total Time 27
Stats: CPU Busy 18 (66.67%)
Stats: IO Busy  12 (44.44%)
```
Process 0 does not get CPU after its 1st IO task completes, and instead waits until all other processes finish. 

It is not efficient. After Process 0 completes its 1st IO task, it could initialize its 2nd IO task, and then let Process 2 have the CPU while Process 0 works on the 2nd IO task. Same with Process 0's 3rd IO task and Process 3.

### 7. Now run the same processes, but with `-I IO_RUN_IMMEDIATE` set, which immediately runs the process that issued the I/O. How does this behavior differ? Why might running a process that just completed an I/O again be a good idea?  

```
python process-run.py -l 3:0,5:100,5:100,5:100 -S SWITCH_ON_IO -I IO_RUN_IMMEDIATE -c -p
Time     PID: 0     PID: 1     PID: 2     PID: 3        CPU        IOs 
  1      RUN:io      READY      READY      READY          1            
  2     WAITING    RUN:cpu      READY      READY          1          1 
  3     WAITING    RUN:cpu      READY      READY          1          1 
  4     WAITING    RUN:cpu      READY      READY          1          1 
  5     WAITING    RUN:cpu      READY      READY          1          1 
  6*     RUN:io      READY      READY      READY          1            
  7     WAITING    RUN:cpu      READY      READY          1          1 
  8     WAITING       DONE    RUN:cpu      READY          1          1 
  9     WAITING       DONE    RUN:cpu      READY          1          1 
 10     WAITING       DONE    RUN:cpu      READY          1          1 
 11*     RUN:io       DONE      READY      READY          1            
 12     WAITING       DONE    RUN:cpu      READY          1          1 
 13     WAITING       DONE    RUN:cpu      READY          1          1 
 14     WAITING       DONE       DONE    RUN:cpu          1          1 
 15     WAITING       DONE       DONE    RUN:cpu          1          1 
 16*       DONE       DONE       DONE    RUN:cpu          1            
 17        DONE       DONE       DONE    RUN:cpu          1            
 18        DONE       DONE       DONE    RUN:cpu          1            

Stats: Total Time 18
Stats: CPU Busy 18 (100.00%)
Stats: IO Busy  12 (66.67%)
```
The `IO_RUN_IMMEDIATE` flag lets the CPU to pause any work to initialize IO as soon as an IO is ready. This is a good idea and guarantees that IO always runs while CPU is busy on something else.

### 8. Now run with some randomly generated processes: `-s 1 -l 3:50,3:50` or `-s 2 -l 3:50,3:50` or `-s 3 -l 3:50,3:50`. See if you can predict how the trace will turn out. What happens when you use the flag `-I IO_RUN_IMMEDIATE` vs. `-I IO_RUN_LATER`? What happens when you use `-S SWITCH_ON_IO` vs. `-S SWITCH_ON_END`?

I guess the CPU usage is going to be roughly 50% of the time, because both processes are doing 50% CPU work and 50% IO work without properly managing it. The result is close to what I guessed:

```
python process-run.py -s 3 -l 3:50,3:50 -c -p
Time     PID: 0     PID: 1        CPU        IOs 
  1     RUN:cpu      READY          1            
  2      RUN:io      READY          1            
  3     WAITING     RUN:io          1          1 
  4     WAITING    WAITING                     2 
  5     WAITING    WAITING                     2 
  6     WAITING    WAITING                     2 
  7*    RUN:cpu    WAITING          1          1 
  8*       DONE     RUN:io          1            
  9        DONE    WAITING                     1 
 10        DONE    WAITING                     1 
 11        DONE    WAITING                     1 
 12        DONE    WAITING                     1 
 13*       DONE    RUN:cpu          1            

Stats: Total Time 13
Stats: CPU Busy 6 (46.15%)
Stats: IO Busy  9 (69.23%)
```

I do not observe much difference after using `-I IO_RUN_IMMEDIATE` nor `-I IO_RUN_LATER` nor `-S SWITCH_ON_IO`:
```
python process-run.py -s 3 -l 3:50,3:50 -c -p -I IO_RUN_IMMEDIATE
Time     PID: 0     PID: 1        CPU        IOs 
  1     RUN:cpu      READY          1            
  2      RUN:io      READY          1            
  3     WAITING     RUN:io          1          1 
  4     WAITING    WAITING                     2 
  5     WAITING    WAITING                     2 
  6     WAITING    WAITING                     2 
  7*    RUN:cpu    WAITING          1          1 
  8*       DONE     RUN:io          1            
  9        DONE    WAITING                     1 
 10        DONE    WAITING                     1 
 11        DONE    WAITING                     1 
 12        DONE    WAITING                     1 
 13*       DONE    RUN:cpu          1            

Stats: Total Time 13
Stats: CPU Busy 6 (46.15%)
Stats: IO Busy  9 (69.23%)
==============================
python process-run.py -s 3 -l 3:50,3:50 -c -p -I IO_RUN_LATER
Time     PID: 0     PID: 1        CPU        IOs 
  1     RUN:cpu      READY          1            
  2      RUN:io      READY          1            
  3     WAITING     RUN:io          1          1 
  4     WAITING    WAITING                     2 
  5     WAITING    WAITING                     2 
  6     WAITING    WAITING                     2 
  7*    RUN:cpu    WAITING          1          1 
  8*       DONE     RUN:io          1            
  9        DONE    WAITING                     1 
 10        DONE    WAITING                     1 
 11        DONE    WAITING                     1 
 12        DONE    WAITING                     1 
 13*       DONE    RUN:cpu          1            

Stats: Total Time 13
Stats: CPU Busy 6 (46.15%)
Stats: IO Busy  9 (69.23%)
```

I do not observe much difference after using `-S SWITCH_ON_IO`, but using `-S SWITCH_ON_END` definitely slowed things down and made CPU and IO both less effecient:

```
python process-run.py -s 3 -l 3:50,3:50 -c -p -S SWITCH_ON_IO
Time     PID: 0     PID: 1        CPU        IOs 
  1     RUN:cpu      READY          1            
  2      RUN:io      READY          1            
  3     WAITING     RUN:io          1          1 
  4     WAITING    WAITING                     2 
  5     WAITING    WAITING                     2 
  6     WAITING    WAITING                     2 
  7*    RUN:cpu    WAITING          1          1 
  8*       DONE     RUN:io          1            
  9        DONE    WAITING                     1 
 10        DONE    WAITING                     1 
 11        DONE    WAITING                     1 
 12        DONE    WAITING                     1 
 13*       DONE    RUN:cpu          1            

Stats: Total Time 13
Stats: CPU Busy 6 (46.15%)
Stats: IO Busy  9 (69.23%)
=================================
python process-run.py -s 3 -l 3:50,3:50 -c -p -S SWITCH_ON_END
Time     PID: 0     PID: 1        CPU        IOs 
  1     RUN:cpu      READY          1            
  2      RUN:io      READY          1            
  3     WAITING      READY                     1 
  4     WAITING      READY                     1 
  5     WAITING      READY                     1 
  6     WAITING      READY                     1 
  7*    RUN:cpu      READY          1            
  8        DONE     RUN:io          1            
  9        DONE    WAITING                     1 
 10        DONE    WAITING                     1 
 11        DONE    WAITING                     1 
 12        DONE    WAITING                     1 
 13*       DONE     RUN:io          1            
 14        DONE    WAITING                     1 
 15        DONE    WAITING                     1 
 16        DONE    WAITING                     1 
 17        DONE    WAITING                     1 
 18*       DONE    RUN:cpu          1            

Stats: Total Time 18
Stats: CPU Busy 6 (33.33%)
Stats: IO Busy  12 (66.67%)
```
